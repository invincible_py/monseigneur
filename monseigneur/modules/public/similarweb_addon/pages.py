from monseigneur.core.browser.pages import HTMLPage,JsonPage
from monseigneur.core.browser.elements import method, DictElement,ItemElement
from monseigneur.modules.public.similarweb_addon.alchemy.tables import Domain
from monseigneur.core.browser.filters.json import Dict
from monseigneur.core.browser.filters.standard import CleanDate
import json ,os
import datetime
class ApiPage(JsonPage):

    def build_doc(self,text):
        return JsonPage.build_doc(self, text)
    
    @method
    class iter_data(DictElement):
        
        def find_elements(self):
            return [self.page.doc]
        
        class get_article(ItemElement):
            klass=Domain

            def obj_domain(self):
                return Dict('SiteName',default=None)(self)
            
            def obj_global_rank(self):
                return Dict('GlobalRank/Rank',default=None)(self)
            
            def obj_country(self):
                crd=os.path.dirname(os.path.realpath(__file__))
                raw_country=Dict('CountryRank/CountryCode',default=None)(self)
                countries=json.load(open(os.path.join(crd,'countries.json'),encoding="utf8"))
                return countries.get(raw_country,'')
            
            def obj_country_rank(self):
                return Dict('CountryRank/Rank',default=None)(self)
            
            def obj_category(self):
                cat=''
                raw_cat=Dict('CategoryRank/Category',default='')(self)
                if raw_cat:
                    cat=raw_cat.replace('_',' ').replace('/','  > ')
                return cat
            
            def obj_category_rank(self):
                return Dict('CategoryRank/Rank',default=None)(self)
            
            def obj_bounce_rate(self):
                raw_data=Dict('Engagments/BounceRate',default=0)(self)
                return round(float(raw_data) * 100, 2)
            
            def obj_pages_per_visit(self):
                return Dict('Engagments/PagePerVisit',default=None)(self)
            
            def obj_est_monthly_visits(self):
                return Dict('Engagments/Visits',default=None)(self)
            
            def obj_avg_visit_duration(self):
                raw_data=Dict('Engagments/TimeOnSite',default=0)(self)
                return str(datetime.timedelta(seconds=float(raw_data))).split('.')[0]
            
            def obj_top_country_shares(self):
                shares=[]
                raw_tops=Dict('TopCountryShares',default=[])(self)
                for raw_top in raw_tops:
                    val=raw_top.get('Value',0)
                    raw_top['Value']=(round(float(val) * 100, 2))
                    shares.append(raw_top)
                return shares

