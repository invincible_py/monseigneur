# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.tools.backend import Module
from .browser import SimilarWebBrowser

__all__ = ['SimilarWebModule']


class SimilarWebModule(Module):
    NAME = 'SimilarWeb'
    MAINTAINER = u'Sham'
    EMAIL = 'haam896@gmail.com'#'{first}.{last}@lobstr.io'
    BROWSER = SimilarWebBrowser

    def get_domain_data(self,domain):
        return self.browser.get_domain_data(domain)