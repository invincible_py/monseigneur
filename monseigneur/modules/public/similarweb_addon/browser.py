# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.browser import PagesBrowser, URL
from .pages import ApiPage

__all__ = ['SimilarWebBrowser']


class SimilarWebBrowser(PagesBrowser):

    BASEURL = 'https://data.similarweb.com/'

    api_page = URL("/api/v1/data\?domain=(?P<domain>.*)", ApiPage)

    def __init__(self, *args, **kwargs):
        super(SimilarWebBrowser, self).__init__(*args, **kwargs)
        self.session.headers.update({
            'authority': 'data.similarweb.com',
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'none',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
            'X-Extension-Version': '6.9.0'
           })

    def get_domain_data(self, domain):
        self.api_page.go(domain=domain)
        assert self.api_page.is_here()
        return self.page.iter_data()

