from sqlalchemy import Column
from sqlalchemy.dialects.mysql import MEDIUMTEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, INTEGER, TIME, JSON, FLOAT
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Domain(Base):

    __tablename__ = 'domain'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    domain = Column(VARCHAR(100), unique=True, nullable=False)
    url = Column(VARCHAR(350))
    global_rank = Column(INTEGER)
    country = Column(MEDIUMTEXT)
    country_rank=Column(INTEGER)
    category= Column(MEDIUMTEXT)
    category_rank = Column(INTEGER)
    bounce_rate=Column(FLOAT)
    pages_per_visit=Column(FLOAT)
    est_monthly_visits=Column(JSON)
    monthly_visit=Column(INTEGER)
    avg_visit_duration=Column(TIME)
    top_country_shares=Column(JSON)
    


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
