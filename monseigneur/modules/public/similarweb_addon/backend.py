import random
import time
from urllib.parse import urlparse
from mbackend.core.fetcher import Fetcher
from mbackend.core.application import Application
from monseigneur.modules.public.similarweb_addon.alchemy.dao_manager import DaoManager
from monseigneur.modules.public.similarweb_addon.alchemy.tables import Domain
import csv

import pymysql
pymysql.install_as_MySQLdb()

class SmWebBackend(Application):
    APPNAME = "SimilarWeb Addon"
    VERSION = '1.0'
    COPYRIGHT = 'Copyright(C) 2023'
    DESCRIPTION = "Similar web Addon Scraping"
    SHORT_DESCRIPTION = "Scraping Domain metrics from Addon"

    def __init__(self):
        super(SmWebBackend, self).__init__(self.APPNAME)
        self.setup_logging()
        self.fetcher = Fetcher(custom_path='public')
        self.module = self.fetcher.build_backend("similarweb_addon", params={})
        self.dao = DaoManager("similar_web_addon")
        self.session, self.scoped_session = self.dao.get_shared_session()

    def read_csv(self):
        FILE_NAME='similarweb_add_on_test.csv'
        inc=0
        self.done_domains=[]
        self.domains=[]
        with open(FILE_NAME,'r',newline='',encoding='utf-8') as csv_file:
            rows = csv.reader(csv_file)
            for row in rows:
                if inc==0:
                    self.csv_headers=row
                    inc+=1
                    continue
                url=row[1]
                if len(row) > 2 and row[2]:
                    self.done_domains.append(row[2])
                    continue
                self.domains.append(url)

    def get_domain(self, url):
        parsed = urlparse(url)
        return parsed.netloc
    def main(self):
        self.read_csv()
        for url in self.domains:
            domain = self.get_domain(url)
            if domain in self.done_domains:
                continue
            self.done_domains.append(domain)
            datas=self.module.get_domain_data(domain)
            for data in datas:
                data.url = url
                if not self.session.query(Domain).filter(Domain.domain == data.domain).count():
                    self.session.add(data)
                    self.session.commit()
            time.sleep(random.randint(1, 5))

if __name__=="__main__":
    obj=SmWebBackend()
    obj.main()