# -*- coding: utf-8 -*-

# Copyright(C) 2014      Bezleputh
#
# This file is part of weboob.
#
# weboob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# weboob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with weboob. If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

from monseigneur.core.browser.pages import HTMLPage, JsonPage
from monseigneur.core.capabilities.base import Currency as BaseCurrency
from monseigneur.core.browser.filters.standard import CleanText, CleanDecimal
from monseigneur.core.browser.filters.json import Dict
from monseigneur.contrib.capabilities.housing import (UTILITIES, ENERGY_CLASS, ADVERT_TYPES, HOUSE_TYPES)
from monseigneur.core.capabilities.base import NotAvailable

from monseigneur.core.browser.filters.base import ItemNotFound

from monseigneur.core.exceptions import ParseError, BrowserBanned

import dateparser
from datetime import datetime
import json
from pytz import timezone
import math
import re

from mbackend.alchemy.modules.pap_immobilier.tables import Annonce


class HousingListPage(HTMLPage):

    def __init__(self, *args, **kwargs):
        HTMLPage.__init__(self, *args, **kwargs)

    def iter_annonces(self):
        for annonce in self.doc.xpath('//div[@class="search-results-list"]/div[@class="search-list-item"]//a[@class="item-title"]'):
            obj_annonce = Annonce()
            if annonce.xpath("./@name"):
                obj_annonce.annonce_id = CleanDecimal("./@name")(annonce)
                obj_annonce.url = "https://www.pap.fr" + CleanText("./@href")(annonce)
                yield obj_annonce

    def get_nextpage(self):
        return CleanText('//li[@class="next"]/a/@href', default=None)(self.doc)


class HousingDetailsPage(HTMLPage):

    def fill_annonce(self, annonce):
        self.data_script =  CleanText('//script[contains(text(),"var ADTECH")]')(self.doc)
        annonce.region = self.match_and_group('kvlocalisation')
        annonce.annonce_type = self.match_and_group('kvproduit')
        annonce.department_id = self.match_and_group('kvdepartement')
        annonce.asset_type = self.match_and_group('kvtypebien')
        annonce.advert_type = "Personal"
        annonce.title = CleanText('//h1[@class="item-title"]/text()')(self.doc)
        picture_list = []

        for picture in self.doc.xpath('//div[contains(@class, "owl-item")]//a/@href'):
            picture_list.append(CleanText(".")(picture))

        annonce.picture = "|||".join(picture_list)
        room_count = self.match_and_group('kvnb_pieces')
        annonce.room_count = self.check_numerics_fields(room_count)

        bedroom_count = self.match_and_group('kvnb_pieces')
        annonce.bedroom_count = self.check_numerics_fields(bedroom_count)

        annonce.area = self.match_and_group('kvsurface_min')
        annonce.price = self.match_and_group('kvprix_min')
        annonce.city = self.match_and_group('kvville')
        annonce.DPE = self.match_and_group('kvclasse_energie')
        annonce.details = self.match_and_group('kvoptions')
        annonce.phone = CleanText('//p[contains(@class, "tel-wrapper")]/span[@class="txt-indigo"]')(self.doc)
        annonce.reference = CleanText('//p[@class="item-date"]')(self.doc)
        annonce.last_publication_date = dateparser.parse(annonce.reference.split(" / ")[1])
        return annonce

    def match_and_group(self, attribute):
        regex = "(.*?){}: '(.*?)',"
        match = re.match(regex.format(attribute), self.data_script)
        if match:
            return match.group(2)
        return None

    def check_numerics_fields(self, attribute):
        if not attribute.isnumeric():
            return None
        return attribute
