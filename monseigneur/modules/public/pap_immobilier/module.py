from monseigneur.core.tools.backend import Module

from .browser import PapImmoBrowser

__all__ = ['PapImmoModule']


class PapImmoModule(Module):
    NAME = 'pap immobilier'
    DESCRIPTION = u'search annonces on leboncoin'
    MAINTAINER = u'Simon Rochwerg'
    EMAIL = 'simon.rochwerg@lobstr.io'
    VERSION = '1.0'

    BROWSER = PapImmoBrowser

    # annonce professionnelles
    def iter_buying_annonces(self, url):
        return self.browser.iter_buying_annonces(url)

    def fill_annonce(self, annonce):
        return self.browser.fill_annonce(annonce)

    def get_nextpage(self):
        return self.browser.get_nextpage()
