# -*- coding: utf-8 -*-

# Copyright(C) 2019 Simon Rochwerg

from monseigneur.core.browser import PagesBrowser, URL
from .pages import UserAgentPage

__all__ = ['PagesBrowser']


class UserAgentBrowser(PagesBrowser):

    BASEURL = 'https://techblog.willshouse.com/'

    useragents = URL('/2012/01/03/most-common-user-agents/', UserAgentPage)

    def __init__(self, *args, **kwargs):
        super(UserAgentBrowser, self).__init__(*args, **kwargs)

    def iter_user_agents(self):
        self.useragents.go()
        assert self.useragents.is_here()
        return self.page.iter_user_agents()
