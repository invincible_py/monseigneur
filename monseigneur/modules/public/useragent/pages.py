from monseigeur.core.browser.pages import HTMLPage
from monseigneur.core.browser.filters.standard import CleanText, CleanDecimal

from mbackend.alchemy.modules.public.useragent.tables import UserAgent


class UserAgentPage(HTMLPage):
    def iter_user_agents(self):
        for useragent in self.doc.xpath('//table[contains(@class, "most-common-user-agents")]/tbody/tr'):
            useragent_obj = UserAgent()
            useragent_obj.percent = CleanDecimal('./td[@class="percent"]')(useragent)
            useragent_obj.useragent = CleanText('./td[@class="useragent"]')(useragent)
            useragent_obj.system = CleanText('./td[@class="system"]')(useragent)
            yield useragent_obj
