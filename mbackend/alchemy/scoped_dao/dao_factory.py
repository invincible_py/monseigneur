from sqlalchemy_utils import create_database, database_exists
from sqlalchemy import create_engine, exists
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.orm.exc import NoResultFound

from mbackend.tools.db_tools.decorators import (dbconnect, dbconnect_noexpire, dbconnect_noclosing,
                                               dbconnect_noclosingandsession, dbconnect_sharedsession)

from monseigneur.core.tools.log import getLogger
import json
import os


class DaoFactory:

    try:
        DATA_PATH = os.environ['DATAPATH']
    except KeyError:
        DATA_PATH = '{}/{}'.format(os.environ['HOME'], 'monseigneur/mbackend')

    def __init__(self, database_name, engine_config):

        self.logger = getLogger("dao_logger")
        try:
            with open(os.path.join(self.DATA_PATH, 'config.json')) as json_data_file:
                self.config = json.load(json_data_file)
        except FileNotFoundError:
            assert engine_config

        self.sql_username = self.config["engine_config"]["sql_username"]
        self.sql_password = self.config["engine_config"]["sql_password"]

        if engine_config is None:
            self.engine_config = 'mysql://{0}:{1}@localhost/{2}?charset=utf8mb4'.format(self.sql_username,
                                                                                        self.sql_password,
                                                                                        database_name)
        else:
            self.engine_config = engine_config

        self.engine_config = self.engine_config.format(database_name)
        self.engine = create_engine(self.engine_config)
        if not database_exists(self.engine.url):
            create_database(self.engine.url)
        self.session_factory = sessionmaker(bind=self.engine)

    @classmethod
    def getInstance(cls, database_name, engine_config):
        new_instance = cls(database_name, engine_config)
        return new_instance

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    def insert(self, session, instance):
        session.add(instance)
        # object is filled with inserted primary key
        return instance

    def get_if_exists(self, session, klass, instance, id_name):
        existing_instance = session.query(klass).filter(getattr(klass, id_name) == getattr(instance, id_name)).first()
        if existing_instance:
            return existing_instance, True
        return instance, False

    def object_exists(self, session, klass, instance, id_name):
        return session.query(exists().where(getattr(klass, id_name) == getattr(instance, id_name))).scalar()

    def select_by_id(self, session, klass, id_name, id_number):
        assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(getattr(klass, id_name) == id_number).one()
        except NoResultFound:
            session.rollback()
            self.logger.warning("No Row found, return None")
            return None

    def select_multiple_by_id(self, session, klass, id_name, id_number):
        assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(getattr(klass, id_name) == id_number).all()
        except NoResultFound:
            session.rollback()
            self.logger.warning("No Row found, return None")
            return None

    def select_by_ids(self, session, klass, id_names, id_numbers):
        assert isinstance(id_names, list)
        assert isinstance(id_numbers, list)
        assert len(id_names) == len(id_numbers)
        for id_name in id_names:
            assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(*[getattr(klass, id_name) == id_number for (id_name, id_number) in zip(id_names, id_numbers)]).one()
        except NoResultFound:
            self.logger.warning("No Row found, return None")
            return None

    def select_multiple_by_ids(self, session, klass, id_names, id_numbers):
        assert isinstance(id_names, list)
        assert isinstance(id_numbers, list)
        assert len(id_names) == len(id_numbers)
        for id_name in id_names:
            assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(*[getattr(klass, id_name) == id_number for (id_name, id_number) in zip(id_names, id_numbers)]).all()
        except NoResultFound:
            self.logger.warning("No Row found, return None")
            return None

    def select_multiple_by_id(self, session, klass, id_name, id_number):
        assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(getattr(klass, id_name) == id_number).all()
        except NoResultFound:
            session.rollback()
            self.logger.warning("No Row found, return None")
            return None

    def count_by_id(self, session, klass, id_name, id_value):
        assert isinstance(id_name, str)
        assert hasattr(klass, id_name)
        return session.query(klass).filter(getattr(klass, id_name) == id_value).count()

    def count_by_ids(self, session, klass, id_names, id_numbers):
        assert isinstance(id_names, list)
        assert isinstance(id_numbers, list)
        assert len(id_names) == len(id_numbers)
        for id_name in id_names:
            assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(*[getattr(klass, id_name) == id_number for (id_name, id_number) in zip(id_names,id_numbers)]).count()
        except NoResultFound:
            self.logger.warning("No Row found, return None")
            return None

    # to bullshit to delete asap
    def count_by_id_noobject(self, session, parent_object, id_name, id_value):
        assert isinstance(id_name, str)
        assert isinstance(id_value, str)
        assert hasattr(parent_object, id_name)
        return session.query(parent_object).filter(getattr(parent_object, id_name) == id_value).count()

    @dbconnect
    def bulk_save_objects(self, object_list, session):
        session.bulk_save_objects(object_list)

    @dbconnect_sharedsession
    def add_all(self, session, object_list):
        session.add_all(object_list)
