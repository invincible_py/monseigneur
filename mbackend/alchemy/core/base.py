from sqlalchemy import Column
from sqlalchemy.types import DateTime, Integer
from sqlalchemy.ext.declarative import declarative_base, declared_attr

from datetime import datetime
from pytz import timezone


class Base(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    __mysql_charset = 'utf8mb4'

    id = Column(Integer, autoincrement=True, primary_key=True, unique=True)
    scraping_time = Column(DateTime, nullable=False, default=datetime.now(timezone('Europe/Paris')))

    def create_all(engine):
        print("creating databases")
        Base.metadata.create_all(engine)


Base = declarative_base(cls=Base)
