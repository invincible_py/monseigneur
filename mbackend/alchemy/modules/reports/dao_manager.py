from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, database_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(database_name, engine_config)
