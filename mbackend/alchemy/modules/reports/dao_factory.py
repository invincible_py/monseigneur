from alchemy.scoped_dao.dao_factory import DaoFactory
from .tables import create_all

from pytz import timezone

class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)
