from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.dialects.mysql import VARCHAR, BOOLEAN, DATETIME
from mbackend.alchemy.core.base import Base

from sqlalchemy.ext.declarative import declarative_base

AuthBase = declarative_base()


class Profile(Base):

    username = Column(VARCHAR)
    email = Column(VARCHAR, unique=True)
    spreadsheet_id = Column(VARCHAR(100))
    tokens = Column(Integer)
    tokens_used = Column(Integer)
    ftp = Column(BOOLEAN)
    ftp_frequency = Column(Integer)
    ftp_time = Column(DateTime)
    ftp_last_id_sent = Column(Integer)
    ftp_adress = Column(VARCHAR)
    ftp_login = Column(VARCHAR)
    ftp_password = Column(VARCHAR)
    user_id = Column(Integer)


class AuthUser(AuthBase):

    __tablename__ = 'auth_user'

    id = Column(Integer, autoincrement=True, primary_key=True)

    password = Column(VARCHAR(128))
    last_login = Column(DateTime)
    is_superuser = Column(BOOLEAN)
    username = Column(VARCHAR(150), unique=True)
    first_name = Column(VARCHAR(30))
    last_name = Column(VARCHAR(150))
    email = Column(VARCHAR(254))
    is_staff = Column(BOOLEAN)
    is_active = Column(BOOLEAN)
    date_joined = Column(DateTime)


class AuthTokenToken(AuthBase):

    __tablename__ = 'authtoken_token'

    key = Column(VARCHAR(40), unique=True, primary_key=True)
    created = Column(DateTime)
    user_id = Column(Integer)
