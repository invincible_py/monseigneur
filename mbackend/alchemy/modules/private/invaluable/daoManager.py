from .daoFactory import DaoFactory


class DaoManager:

    def __init__(self, db_name, mapper, ssh=None, ip=None, ssh_username=None, ssh_password=None):
        self.df = DaoFactory(db_name, mapper, ssh, ip, ssh_username, ssh_password)

    def select(self, key=None, value=None, column='*', order_by=None, limit=None):
        return self.df.select(key, value, column, order_by, limit)

    def count(self, key, value):
        return self.df.count(key, value)

    def insert(self, _object):
        return self.df.insert(_object)

    def update(self,  _object, key, value, target=None):
        return self.df.update(_object, key, value, target)

    def compare(self, in_tup, _object):
        return self.df.compare(in_tup, _object)

    def rollback(self):
        return self.df.rollback()

    def send_email(self, emailfrom, emailto, username, password, fileToSend=None, subject=None, message=None):
        return self.df.send_email(emailfrom, emailto, username, password, fileToSend, subject, message)

    def count_new_watches_for_the_day(self, day=None, month=None, year=None):
        return self.df.count_new_watches_for_the_day(day, month, year)

    def count_all_watches(self):
        return self.df.count_all_watches()
