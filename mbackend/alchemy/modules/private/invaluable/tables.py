from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT
from sqlalchemy.types import DateTime

Base = declarative_base()


class HistoryWatch(Base):
    __tablename__ = 'watches'

    id = Column(VARCHAR(200), primary_key=True)
    _key = Column(String(200), unique=True)
    url = Column(String(1000))
    title = Column(LONGTEXT)
    catalog_id = Column(TINYTEXT)
    catalog_ref = Column(TINYTEXT)
    lot = Column(TINYTEXT)
    brand = Column(TINYTEXT)
    estimate = Column(TINYTEXT)
    bid_price = Column(TINYTEXT)
    realized = Column(TINYTEXT)
    currency = Column(TINYTEXT)
    bids = Column(TINYTEXT)
    saved_count = Column(TINYTEXT)
    condition = Column(LONGTEXT)
    description = Column(LONGTEXT)
    parsed_description = Column(MEDIUMTEXT)
    date_range = Column(MEDIUMTEXT)
    auction_house = Column(TINYTEXT)
    auction_date = Column(TINYTEXT)
    auction_address = Column(TINYTEXT)
    auction_collection = Column(TINYTEXT)
    publication_date = Column(TINYTEXT)
    page = Column(TINYTEXT)
    images = Column(LONGTEXT)
    logged = Column(Integer)
    scrapped_time = Column(TINYTEXT)
    is_images = Column(TINYINT)
    is_detail = Column(TINYINT)

    def __init__(self, source=None):
        if source is not None:
            self.__dict__.update(source.__dict__)

    def __repr__(self):
        return "<Domain(id='{}', url='{}', title='{}'>".format(HistoryWatch.id, HistoryWatch.url, HistoryWatch.title)


class WatchImage(Base):
    __tablename__ = "watches_image"

    id = Column(VARCHAR(200), primary_key=True, autoincrement=False)
    website_id = 2
    watch_id = Column(VARCHAR(200))
    scraped_time = Column(VARCHAR(200))
    url = Column(String(1000))
    path = Column(String(1000))


class WatchSent(Base):
    __tablename__ = "watches_sent"

    id = Column(VARCHAR(200), autoincrement=False, primary_key=True)
    sent_time = Column(VARCHAR(200))

def create_all(engine):
    print("-- CREATING DB --")
    Base.metadata.create_all(engine, checkfirst=True)
