from sqlalchemy import text
from sqlalchemy import inspect
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker
from .tables import create_all
import MySQLdb

import re
from pytz import timezone
import datetime
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

class DaoFactory:

    """**DaoFactory SQL Alchemy**
        - Initialize a MySQL connection to a MySQL database
        - Create:
            + database if database does not exist
            + table if table does not exist
        - Provide core functions to manipulate table:
            + select
            + insert
            + count
            + update
            + compare
            + rollback (if error)
        - Optionally:
            + initialize SSH tunnel to the database, using ssh username/password
    Keyword Arguments:
        db_name (str):
            Name of the database to be built. If already exist, not created.
            Set MySQL char to utf8mb4.
        sqlobject (object):
            SQLAlchemy-based object, to build table and additional rows.
        ssh (boolean, optionnal):
            If True, initialize an SSH tunnel based on username/password
            couple of strings.
        ip (str, optional):
            IP of the remote server.
        ssh_username (str, optional):
            Username to authenticate in remote server.
        ssh_password (str, optional):
            Text representing the password used to connect to remote
            server.
"""

    def __init__(self, db_name, mapper, ssh=None, ip=None, ssh_username=None, ssh_password=None):
        # COMPULSORY ARGUMENT
        self.db_name = db_name
        self.mapper = mapper

        # OPTIONAL SSH ARGUMENTS
        if ssh:
            self.server = SSHTunnelForwarder(
                ip,
                ssh_username=ssh_username,
                ssh_password=ssh_password,
                remote_bind_address=('127.0.0.1', 3306)
            )
            self.server.start()
            local_port = str(self.server.local_bind_port)
        else:
            local_port = str('3306')

        # CREATE DB IF NEEDED
        engine_config = 'mysql://root:@127.0.0.1:{}'.format(local_port)
        self.engine = create_engine(engine_config)
        self.engine.execute("CREATE DATABASE IF NOT EXISTS `{}` DEFAULT CHARACTER SET utf8mb4 ;".format(self.db_name))
        self.engine.dispose()
        engine_config = 'mysql://root:@127.0.0.1:{}/{}?charset=utf8mb4'.format(local_port, self.db_name)
        self.engine = create_engine(engine_config)
        self.engine.execute("USE {};".format(self.db_name))

        # GET ALCHEMY SESSION
        self.session = sessionmaker(bind=self.engine)

        # CREATE TABLE IF NEEDED
        create_all(self.engine)
        #self.mapper.__table__.create(self.engine, checkfirst=True)

    # TODO ORM-based select
    def select(self, key=None, value=None, column='*', order_by=None, limit=None):
        """
        Select the rows and all columns that match
        with the key/value couple.
        Keyword Arguments:
            key (str):
                list with the key column(s) (not required)
            value(str):
                list that has to be matched (not required)
            column(list):
                list of columns to be selected (not required)
            order_by(list):
                list with key to order, and desc/asc
            limit(int):
                limit the number of lines extracted (not required)

        >>> select()
        SELECT * FROM table_name;
        >>> select(column=['label1', 'label5'])
        SELECT label1, label5 FROM table_name;
        >>> select(key='id', value='150298', column=['label1', 'label5'])
        SELECT label1, label5 FROM table_name WHERE id = '150298';
        >>> select(limit=100)
        SELECT * FROM table_name LIMIT 100;
        """

        where = ''
        _order = ''
        _list = []

        self.s = self.session(autocommit=False, autoflush=False)
        self.s.execute("USE {};".format(self.db_name))

        if column and isinstance(column, list):
            column = ', '.join(column)

        if key and isinstance(key, list) and value and isinstance(value, list):
            assert (len(key) == len(value))
            for _key, _value in list(zip(key, value)):
                if _value.lower() in ['null']:
                    part = '{} IS {}'.format(_key, _value)
                else:
                    part = '{} = {}'.format(_key, _value)
                _list.append(part)
            where = ' WHERE ' + ' AND '.join(_list)

        if order_by and isinstance(order_by, list):
            order_key = order_by[0]
            order_direction = order_by[1].upper()
            _order = ' ORDER BY {} {}'.format(order_key, order_direction)

        if limit and isinstance(limit, int):
            _limit = " LIMIT {}".format(str(limit))
        else:
            _limit = ''

        print(_order)
        t = text(
            "SELECT {} FROM {}.{}".format(column, self.db_name, self.mapper.__tablename__) + where + _order + _limit + ";")
        print(t)
        data = self.s.execute(t).fetchall()
        self.s.close()
        return data

    def insert(self, _object):
        """Insert an SQL Alchemy Object in the db, after
        Merge query, add if not present, update if already in db, one-line-huge :o
        Keyword Arguments:
            _object (object):
                SQL Alchemy-based object
            """
        self.s.execute('USE {};'.format(self.db_name))
        self.s.add(_object)
        self.s.flush()
        self.s.commit()

    #TODO ORM-based count
    def count(self, key, value):
        """
        Count the number of elements in the db
        that match with indicated key/value couple.
        Return True if an object is already inserted
        in the table, and False otherwise.
        Keyword Arguments:
            key (list):
                name of the columns from the db
            value (list):
                values that has to be matched
        >>> obj = Object(id=5)
        >>> bool('id', 5)
        >>> True
        """

        where = ''
        _list = []
        self.s = self.session(autocommit=False, autoflush=False)
        print(key)
        print(value)
        print(list(zip(key, value)))
        if key and isinstance(key, list) and value and isinstance(value, list):
            assert(len(key) == len(value))
            for _key, _value in list(zip(key, value)):
                if _value.lower() in ['null']:
                    part = '{} IS {}'.format(_key, _value)
                elif '%' in _value.lower():
                    part = "{} LIKE '{}'".format(_key, _value)
                else:
                    part = "{} = '{}'".format(_key, _value)
                _list.append(part)
            where = ' WHERE ' + ' AND '.join(_list)

        t = text("SELECT COUNT(*) FROM {}.{}".format(self.db_name, self.mapper.__tablename__) + where + ";")
        print(t)
        scalar = self.s.execute(t).scalar()
        self.s.flush()
        self.s.commit()
        self.s.close()
        return scalar

    def update(self, _object, key, value, target=None):
        """
        Update all the values of a row in the db
        that match with key/value couple. All values
        are replaces by values from _object.
        Keyword Arguments:
            _object (object):
                sqlalchemy object
            key (str):
                string with the key column
            value(str):
                value that has to be matched
            target(list):
                column(s) to update specifically (not required)
                if not mentioned, update the all columns
        """

        item_list = []
        self.s = self.session(autocommit=False, autoflush=False)

        for k, v in {c.key: getattr(_object, c.key) for c in inspect(_object).mapper.column_attrs}.items():
            if not target:
                item_list.append(k + "=" + "'" + self.escape(str(v)) + "'")
            if target and isinstance(target, list):
                if k in target:
                    item_list.append(k + "=" + "'" + self.escape(str(v)) + "'")

        query = "UPDATE " + "{}.{}".format(self.db_name, self.mapper.__tablename__) + " SET " + ", ".join(item_list) \
                    + " WHERE " + key + " = " + "'" + value + "'"


        t = text(query)
        self.s.execute(t)
        self.s.flush()
        self.s.commit()
        self.s.close()

    def compare(self, in_tup, _object):
        """
        Compare the value of the sqlalchemy _object,
        with a value already inserted in db.
        Keyword Arguments:
            in_tup (tuple):
                tuple with all the values of a row within the db
            _object(object):
                sqlalchemy object"""

        in_tup = tuple(map(str, in_tup))  # tuple(map(...)) stringifies every field for easy comp.
        out_tup = tuple(map(str,
                            tuple(getattr(_object, c.key) for c in inspect(_object).mapper.column_attrs)))
        return in_tup == out_tup

    def rollback(self):
        """
        Rollback session in case of errors.
        >>> try:
        >>>         <use session>
        >>>         session.commit()
        >>> except:
        >>>         session.rollback()
        >>>         raise
        """
        return self.s.rollback()

    def send_email(self, emailfrom, emailto, username, password, fileToSend=None, subject=None, message=None):

        # TODO clean docstring
        '''
        Automatically send mail to clients through
        SMTP server.

        Keyword Arguments:
            emailfrom (str):
                email of the sender of the mail
            emailto(list):
                list of the receivers of the mail
            username (str):
                username of the mail account
            password (str):
                password of the mail account
            fileToSend (str):
                path of the file to be sent (not required)
            subject (str):
                Subject of the mail (not required)
            message (str):
                message of the text (not required)
        """


        emailto = "destination@example.com"
        fileToSend = "hi.csv"
        username = "user"
        password = "password"
        '''

        assert isinstance(emailfrom, str)
        assert isinstance(emailto, list)
        assert isinstance(username, str)
        assert isinstance(password, str)

        start = datetime.datetime.now()
        print('START SENDING MAIL: {}'.format(str(start)))

        msg = MIMEMultipart()
        msg["From"] = emailfrom
        msg["To"] = ', '.join(emailto)
        if subject and isinstance(subject, str):
            msg["Subject"] = subject

        if message and isinstance(message, str):
            body = message
            msg.attach(MIMEText(body, 'plain'))

        if fileToSend:
            ctype, encoding = mimetypes.guess_type(fileToSend)
            if ctype is None or encoding is not None:
                ctype = "application/octet-stream"

            maintype, subtype = ctype.split("/", 1)

            if maintype == "text":
                fp = open(fileToSend)
                attachment = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "image":
                fp = open(fileToSend, "rb")
                attachment = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "audio":
                fp = open(fileToSend, "rb")
                attachment = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(fileToSend, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)
            attachment.add_header("Content-Disposition", "attachment", filename=os.path.basename(fileToSend))
            msg.attach(attachment)

        server = smtplib.SMTP("smtp.gmail.com:587")
        server.ehlo()
        server.starttls()
        server.login(username, password)
        server.sendmail(emailfrom, emailto, msg.as_string())
        server.quit()

        end = datetime.datetime.now()
        print('END SENDING MAIL: {}'.format(str(end)))
        print('TIME ELAPSED: {}'.format(str(end - start)))


    def count_new_watches_for_the_day(self, day, month, year):
        self.timezone = timezone('Europe/Paris')
        if all([day, day, year]):
            start = datetime.datetime.date(year, day, month)
            start = start.replace(hour=0, minute=0, second=1)
            end = start.replace(hour=23, minute=59, second=59)
        else:
            start = datetime.datetime.now(self.timezone).replace(hour=0, minute=0, second=1)
            end = start.replace(hour=23, minute=59, second=59)
        #return self.s.query(func.count(Watch.scraped_time > start and Watch.scraped_time < end))
        t = text("SELECT COUNT(*) FROM {}.{}".format(self.db_name, self.mapper.__tablename__) + " where scrapped_time > '{}' and scrapped_time < '{}'".format(start, end))
        print(t)
        self.s = self.session(autocommit=False, autoflush=False)

        return self.s.execute(t).scalar()


    def count_all_watches(self):
        #return self.s.query(func.count(Watch.scraped_time > start and Watch.scraped_time < end))
        t = text("SELECT COUNT(*) FROM {}.{}".format(self.db_name, self.mapper.__tablename__) + " where scrapped_time is NOT NULL")
        print(t)
        self.s = self.session(autocommit=False, autoflush=False)

        return self.s.execute(t).scalar()

    def escape(self, _str):
        _str = re.escape(_str)
        if "'" in _str:
            _str = re.sub(r'\'', r"\\'", _str)
        return _str
