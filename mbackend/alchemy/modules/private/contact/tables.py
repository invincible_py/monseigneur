from sqlalchemy import Column, Integer, DateTime

from sqlalchemy.dialects.mysql import MEDIUMTEXT, BOOLEAN, LONGTEXT, VARCHAR, DATETIME, FLOAT, JSON, TINYINT, INTEGER
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Contact(Base):
    __tablename__ = 'contact'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    mysql_charset = 'utf8mb4'

    id = Column(Integer, primary_key=True, autoincrement=True)

    value = Column(MEDIUMTEXT)
    type = Column(MEDIUMTEXT)  # mail or phone
    usage = Column(MEDIUMTEXT)  # personal or professional
    source = Column(MEDIUMTEXT)
    domain = Column(MEDIUMTEXT)
    ref_id = Column(INTEGER)
    extracted_on = Column(DateTime)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
