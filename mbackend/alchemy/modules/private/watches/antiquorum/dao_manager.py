from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, session, alchemy_object, instance_object, id_name):
        return self.daoFactory.select_by_id(session, alchemy_object, instance_object, id_name)

    def select_monitoring(self):
        return self.daoFactory.select_monitoring()

    def count_by_id(self, session, parent_object, child_object, id_name):
        return self.daoFactory.count_by_id(session, parent_object, child_object, id_name)
