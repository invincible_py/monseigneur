from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def count_auction(self, auction_id):
        return self.daoFactory.count_auction(auction_id)

    def auction_is_scraped(self, session, auction_id):
        return self.daoFactory.auction_is_scraped(session, auction_id)

    def count_watch(self, watch_id):
        return self.daoFactory.count_watch(watch_id)

    def count_image(self, image_id):
        return self.daoFactory.count_image(image_id)

    def count_contact(self, auction_id, mail):
        return self.daoFactory.count_contact(auction_id, mail)

    def select_contact(self, session, watch_id, mail):
        return self.daoFactory.select_contact(session, watch_id, mail)
