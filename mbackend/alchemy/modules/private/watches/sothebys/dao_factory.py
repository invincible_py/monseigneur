from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory
from mbackend.alchemy.modules.private.watches.tables import Watch, Auction, Image, Contact, AuctionContact

from mbackend.alchemy.modules.private.watches.tables import create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_, or_


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect
    def count_auction(self, session, auction_id):
        return session.query(Auction).filter(Auction.auction_id == auction_id).count()

    def auction_is_scraped(self, session, auction_id):
        auction = session.query(Auction).filter(Auction.auction_id == auction_id).one()
        if auction.is_done == 1:
            return True
        return False

    @dbconnect
    def count_watch(self, session, watch_id):
        return session.query(Watch).filter(Watch.watch_id == watch_id).count()

    @dbconnect
    def count_image(self, session, image_id):
        return session.query(Image).filter(Image.image_id == image_id).count()

    @dbconnect
    def count_contact(self, session, auction_id, mail):
        return session.query(AuctionContact).filter(AuctionContact.mail == mail, AuctionContact.auction_id == auction_id).count()

    def select_contact(self, session, watch_id, mail):
        return session.query(AuctionContact).filter(AuctionContact.mail == mail, AuctionContact.auction_id == watch_id).one()
