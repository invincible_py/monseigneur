from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, FLOAT, BOOLEAN
from sqlalchemy.types import DateTime
from mbackend.alchemy.core.base import Base


class Auction(Base):
    __tablename__ = "auction"

    id = Column(Integer, primary_key=True, autoincrement=True)

    auction_id = Column(VARCHAR(300), primary_key=True, unique=True)
    slug = Column(VARCHAR(300))

    date = Column(DateTime)
    start_date = Column(DateTime)
    end_date = Column(DateTime)

    title = Column(VARCHAR(200))
    description = Column(MEDIUMTEXT)
    department = Column(MEDIUMTEXT)
    location = Column(VARCHAR(200))
    permalink = Column(VARCHAR(200), unique=True)
    year = Column(Integer)
    month = Column(Integer)
    image = Column(VARCHAR(500))

    currency = Column(VARCHAR(200))
    total = Column(Integer)

    is_in_progress = Column(BOOLEAN, default=0)  # if auction still live

    last_item = Column(Integer)  # last item scraped, to compare with total (monitoring feature)
    total_items = Column(Integer)  #
    last_page_scraped = Column(Integer)

    # in case date is a range or an information string
    # bad practice
    alt_date = Column(VARCHAR(200))
    is_done = Column(BOOLEAN)

    auction_watches = relationship("Watch")
    auction_contact = relationship("AuctionContact")


class Watch(Base):
    __tablename__ = 'watches'

    id = Column(Integer, primary_key=True, autoincrement=True)
    watch_id = Column(VARCHAR(300), primary_key=True, unique=True)
    auction_id = Column(VARCHAR(300), ForeignKey('auction.auction_id'))

    ### GENERAL FEATURES ###
    url = Column(MEDIUMTEXT)
    ref = Column(VARCHAR(200))
    title = Column(MEDIUMTEXT)
    subtitle = Column(MEDIUMTEXT)
    low_estimate = Column(Integer)
    high_estimate = Column(Integer)
    realized = Column(Integer)

    is_closed = Column(BOOLEAN)
    is_sold = Column(BOOLEAN)
    total_bids = Column(Integer)

    currency = Column(VARCHAR(200))
    lot = Column(VARCHAR(200))  # lot number
    brand = Column(VARCHAR(200))
    brand_id = Column(VARCHAR(200))
    description = Column(MEDIUMTEXT)
    grade = Column(MEDIUMTEXT)
    year = Column(Integer)
    year_string = Column(VARCHAR(200))
    other_information = Column(MEDIUMTEXT)

    ### TECHNICAL FEATURES ###
    model = Column(MEDIUMTEXT)
    mov = Column(VARCHAR(800))
    case = Column(MEDIUMTEXT)
    case_number = Column(VARCHAR(200))
    material = Column(MEDIUMTEXT)
    closure = Column(MEDIUMTEXT)
    crystal = Column(VARCHAR(200))
    calibre = Column(MEDIUMTEXT)
    caliber = Column(VARCHAR(800))
    bracelet = Column(MEDIUMTEXT)
    clasp = Column(MEDIUMTEXT)
    accessories = Column(MEDIUMTEXT)
    notes = Column(MEDIUMTEXT)
    dial = Column(MEDIUMTEXT)
    specificities = Column(MEDIUMTEXT)
    signature = Column(MEDIUMTEXT)
    movement = Column(MEDIUMTEXT)
    movement_number = Column(VARCHAR(500))

    circa = Column(VARCHAR(200))
    condition = Column(MEDIUMTEXT)
    height = Column(FLOAT)
    width = Column(FLOAT)
    depth = Column(FLOAT)
    diameter = Column(FLOAT)
    length = Column(FLOAT)

    dimensions = Column(VARCHAR(200))
    provenance = Column(MEDIUMTEXT)
    exhibited = Column(MEDIUMTEXT)
    literature = Column(MEDIUMTEXT)
    essay = Column(LONGTEXT)

    watch_images = relationship("Image")
    watch_artist = relationship("Artist")
    watch_contact = relationship("Contact")

    # in case realized is a range or an information string
    alt_realized = Column(VARCHAR(200))
    scraping_date = Column(DateTime)
    page = Column(Integer)
    is_withdrawn = Column(BOOLEAN)


class Image(Base):
    __tablename__ = "image"

    id = Column(Integer, primary_key=True, autoincrement=True)
    watch_id = Column(VARCHAR(300), ForeignKey('watches.watch_id'))
    image_id = Column(VARCHAR(200))
    alt = Column(MEDIUMTEXT)  # ?

    url = Column(VARCHAR(1000))
    path = Column(VARCHAR(1000))

    scraped_time = Column(DateTime)


class Artist(Base):
    __tablename__ = 'artist'

    id = Column(Integer, primary_key=True, autoincrement=True)
    artist_id = Column(VARCHAR(200))

    watch_id = Column(VARCHAR(300), ForeignKey('watches.watch_id'))

    name = Column(VARCHAR(200))
    nationality = Column(VARCHAR(200))
    birth_year = Column(VARCHAR(200))
    bio = Column(LONGTEXT)


class Contact(Base):
    __tablename__ = 'contact'

    id = Column(Integer, primary_key=True, autoincrement=True)
    watch_id = Column(VARCHAR(300), ForeignKey('watches.watch_id'))

    name = Column(MEDIUMTEXT)
    position = Column(MEDIUMTEXT)
    phone = Column(MEDIUMTEXT)
    mail = Column(MEDIUMTEXT)
    full_text = Column(MEDIUMTEXT)


class AuctionContact(Base):
    __tablename__ = 'auction_contact'

    id = Column(Integer, primary_key=True, autoincrement=True)
    auction_id = Column(VARCHAR(300), ForeignKey('auction.auction_id'))

    name = Column(MEDIUMTEXT)
    position = Column(MEDIUMTEXT)
    phone = Column(MEDIUMTEXT)
    mail = Column(MEDIUMTEXT)
    full_text = Column(MEDIUMTEXT)


class Monitoring(Base):
    __tablename__ = "monitoring"

    id = Column(Integer, primary_key=True, autoincrement=True)
    total_pages = Column(Integer)
    last_page_scraped = Column(Integer)
    last_scraping_date = Column(DateTime)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
