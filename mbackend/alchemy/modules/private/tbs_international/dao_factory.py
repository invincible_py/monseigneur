from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_, or_
from .tables import *
import datetime


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect
    def insert(self, session, instance_object):
        session.add(instance_object)

    # Part 1 methods
    @dbconnect
    def count_category_url(self, session, alchemy_object, category_url):
        return session.query(alchemy_object).filter(alchemy_object.category_url == category_url).count()

    @dbconnect
    def count_product_url(self, session, alchemy_object, product_url):
        return session.query(alchemy_object).filter(alchemy_object.product_url == product_url).count()

    # Part 2 methods
    @dbconnect
    def get_unscraped_products_count(self, session, alchemy_object):
        return session.query(alchemy_object).filter(alchemy_object.is_scraped == 0).count()

    @dbconnect
    def get_unscraped_products_list(self, session, alchemy_object):
        return session.query(alchemy_object).filter(alchemy_object.is_scraped == 0)

    @dbconnect
    def product_count(self, session, alchemy_object, product_url):
        return session.query(alchemy_object).filter(alchemy_object.url == product_url).count()

    @dbconnect
    def set_as_scraped(self, session, alchemy_object, product_url):
        session.expire_on_commit = False
        session.query(alchemy_object).filter(alchemy_object.product_url == product_url).update({"is_scraped": 1})
        session.commit()

    @dbconnect
    def get_product_id(self, session, alchemy_object, unique_value):
        return session.query(alchemy_object.id).filter(alchemy_object.url == unique_value).one()

    @dbconnect
    def get_id_through_link(self, session, alchemy_object, link):
        return session.query(alchemy_object.id).filter(alchemy_object.product_url == link).one()

    @dbconnect
    def remove_broken_link(self, session, alchemy_object, id):
        session.query(alchemy_object).filter(alchemy_object.id == id).delete()
