from sqlalchemy import Column, Integer

from sqlalchemy.dialects.mysql import LONGTEXT, VARCHAR, BOOLEAN
from sqlalchemy.types import DateTime

from mbackend.alchemy.core.base import Base


class Product(Base):
    __tablename__ = "products"

    id = Column(Integer, primary_key=True)
    url = Column(VARCHAR(length=750), unique=True)
    fil = Column(VARCHAR(length=500))
    title = Column(VARCHAR(length=500))
    price = Column(VARCHAR(length=100))
    description = Column(LONGTEXT)
    brand = Column(VARCHAR(length=500))
    documents = Column(LONGTEXT)
    images = Column(LONGTEXT)
    logo = Column(VARCHAR(length=500))
    scraping_time = Column(DateTime)


class Variantes(Base):
    __tablename__ = "variantes"

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer)
    position = Column(Integer)
    content = Column(LONGTEXT)


class CategoryLinks(Base):
    __tablename__ = "category_links"

    id = Column(Integer, primary_key=True)
    category_url = Column(VARCHAR(length=750), unique=True)
    scraping_time = Column(DateTime)


class ProductLinks(Base):
    __tablename__ = "product_links"

    id = Column(Integer, primary_key=True)
    product_url = Column(VARCHAR(length=750), unique=True)
    is_scraped = Column(BOOLEAN, default=False)
    scraping_time = Column(DateTime)


class BrokenLinks(Base):
    __tablename__ = "broken_links"

    id = Column(Integer, primary_key=True)
    url = Column(VARCHAR(length=750))


def create_all(engine):
    print("Creating databases")
    Base.metadata.create_all(engine)
