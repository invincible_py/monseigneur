from sqlalchemy import Column, String, Integer, ForeignKey, Table, Float
from mbackend.alchemy.core.base import Base
from mbackend.alchemy.objects.real_estate.annonce import Annonce


from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, DECIMAL, BOOLEAN
from sqlalchemy.types import DateTime, Float, String
from sqlalchemy.orm import relationship

from mbackend.alchemy.objects.real_estate.monitoring import Monitoring


class Monitoring(Monitoring, Base):
    last_url_scraped = Column(MEDIUMTEXT)


class Annonce(Annonce, Base):

    monitoring_id = Column(Integer, ForeignKey('monitoring.id'))
    monitoring = relationship("Monitoring")

    bedroom_count = Column(Integer)
    reference = Column(VARCHAR(200))
    page_scraped = Column(Integer)

    annonce_phone_id = Column(Integer, ForeignKey('annonce_phone.id'))
    annonce_phone = relationship("AnnoncePhone")


class AnnoncePhone(Base):
    __tablename__ = 'annonce_phone'

    id = Column(Integer, primary_key=True)
    phone = Column(MEDIUMTEXT)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
