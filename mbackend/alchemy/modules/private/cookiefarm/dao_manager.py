from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None, tables=None):
        super(DaoManager, self).__init__()
        self.dao_factory = DaoFactory(table_name, engine_config)

    def get_cookie_session(self):
        return self.dao_factory.get_cookie_session()

    def get_existing_cookie_session(self):
        return self.dao_factory.get_existing_cookie_session()

    def insert_cookie_session(self):
        return self.dao_factory.insert_cookie_session()
