from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None, tables=None):
        super(DaoManager, self).__init__()
        self.dao_factory = DaoFactory(table_name, engine_config)

    def get_cookie_session(self):
        pass

    def state_from_shelf(self):
        pass
