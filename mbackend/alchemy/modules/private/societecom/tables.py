from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.types import String
from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT


class Base(object):
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    __mysql_charset = 'utf8mb4'


Base = declarative_base(Base)


class Company(Base):
    __tablename__ = "company"

    # increase length to prevent DataTooLong ColumnError

    id = Column(Integer, primary_key=True, autoincrement=True)
    denomination = Column(String(length=500))
    adresse = Column(String(length=500))
    siren = Column(String(length=500))
    siret = Column(String(length=500), unique=True)
    activite = Column(String(length=200))
    forme_juridique_rcs = Column(String(length=500))
    date_immatriculation = Column(String(length=500))
    date_de_derniere = Column(String(length=500))
    tranche_deffectif = Column(String(length=500))
    capital_social = Column(String(length=500))
    rcs = Column(String(length=500))
    code_greffe = Column(String(length=500))
    n_dossier = Column(String(length=500))
    nom = Column(String(length=500))
    complement_nom = Column(String(length=500))
    adresse_2 = Column(String(length=500))
    code_postal = Column(String(length=500))
    ville = Column(String(length=500))
    pays = Column(String(length=500))
    categorie = Column(String(length=500))
    forme_juridique_insee = Column(String(length=500))
    code_ape_de_lentreprise = Column(String(length=500))
    activite_code_naf_ou_ape = Column(String(length=500))
    code_ape_du_siege = Column(String(length=500))
    activite_du_siege = Column(String(length=500))
    date_immatruculation_rcs = Column(String(length=500))
    date_creation_entreprise = Column(String(length=500))
    date_creation_siege_actuel = Column(String(length=500))
    type_mandataire = Column(String(length=500))
    date_mandataire = Column(MEDIUMTEXT)
    genre_mandataire = Column(String(length=500))
    nom_mandataire = Column(String(length=500))
    url = Column(String(length=500))
    status = Column(Integer)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
