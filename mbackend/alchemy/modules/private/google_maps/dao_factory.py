from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all
from .tables import Monitoring, RawResult

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_

import coloredlogs, logging
coloredlogs.install()

class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect_noexpire
    def select_monitoring_not_done(self, session, _id):
        return session.query(Monitoring).filter(Monitoring.id == _id).one()

    @dbconnect_noexpire
    def select_all_monitoring_not_done(self, session):
        return session.query(Monitoring).filter(Monitoring.is_done == 0).all()

    @dbconnect
    def count_monitoring(self, session, location, keyword):
        logging.info("Now counting %s %s" % (location, keyword))
        return session.query(Monitoring).filter(
            and_(Monitoring.location == location, Monitoring.keyword == keyword)).count()

    @dbconnect
    def count_monitoring_more(self, session, alchemy_object, location, keyword):
        return session.query(alchemy_object).filter(
            and_(alchemy_object.location == location, alchemy_object.keyword == keyword,
                 alchemy_object.is_done == 1)).count()

    @dbconnect
    def update_monitoring(self, session, keyword, location):
        obj = session.query(Monitoring).filter(
            and_(Monitoring.location == location, Monitoring.keyword == keyword)).one()
        obj.is_done = 1

    @dbconnect
    def count_result(self, session, _zero_x):
        logging.info("Now counting %s" % _zero_x)
        return session.query(RawResult).filter(RawResult.zero_x == _zero_x).count()

    @dbconnect
    def insert(self, session, alchemy_object):
        logging.info("Now inserting %s" % alchemy_object.__dict__)
        return session.add(alchemy_object)

