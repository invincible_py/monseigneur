from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory
from .tables import Monitoring


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    def select_monitoring_not_done(self, _id):
        return self.daoFactory.select_monitoring_not_done(_id)

    def select_all_monitoring_not_done(self):
        return self.daoFactory.select_all_monitoring_not_done()

    def count_monitoring(self, location, keyword):
        return self.daoFactory.count_monitoring(location, keyword)

    def count_monitoring_more(self, alchemy_object, location, keyword):
        return self.daoFactory.count_monitoring_more(alchemy_object, location, keyword)

    def update_monitoring(self, location, keyword):
        return self.daoFactory.update_monitoring(location, keyword)

    def count_result(self, _zero_x):
        return self.daoFactory.count_result(_zero_x)

    def insert(self, alchemy_object):
        return self.daoFactory.insert(alchemy_object)


