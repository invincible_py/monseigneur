from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory
from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession

from .tables import Website, BrandName, Brand, Watch, WatchImage
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import update
from sqlalchemy.sql.expression import exists
from sqlalchemy import or_, and_
from sqlalchemy import func
from sqlalchemy import distinct

import re
import time
from pytz import timezone
from datetime import datetime, timedelta


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)

    def update_last_scraped_time(self, table_name, id):
        query = update(Watch).where(Watch.id == id).\
                values(scraped_time="{}".format(datetime.now(self.timezone).strftime('%Y-%m-%d %H:%M:%S')))
        self.connection.execute(query)

    def update_last_scraped_time_for_brand(self, brand_id):
        query = update(Brand).where(Brand.id == brand_id).values(
            last_scraped_time="{}".format(datetime.now(self.timezone).strftime('%Y-%m-%d %H:%M:%S')))
        self.connection.execute(query)

    @dbconnect
    def get_last_scraped_date(self, session, brand_id):
        return session.query(Brand.last_scraped_time).filter(Brand.id == brand_id).one()[0]

    def reset_last_page_scraped(self):
        accepted_scraped_age = datetime.now(self.timezone) - timedelta(hours=12)
        query = update(Brand).where(Brand.last_scraped_time <
            accepted_scraped_age).values(last_page_scraped=1)
        self.connection.execute(query)

    @dbconnect
    def get_brands_to_scrape(self, session, website_id, max_page_to_scrape=None):
        if max_page_to_scrape:
            return session.query(Brand).filter(Brand.website_id == website_id and Brand.last_page_scraped < max_page_to_scrape).order_by(Brand.last_scraped_time.asc())
        return session.query(Brand).filter(Brand.website_id == website_id).order_by(Brand.last_scraped_time.asc())

    @dbconnect
    def get_chrono24_brand_list(self, session, website_id=1):
        return session.query(BrandName.id, BrandName.brand_name).all()


    @dbconnect
    def get_brand_id(self, session, brand_name):
        return session.query(BrandName.id).filter(BrandName.brand_name == brand_name).one()[0]

    @dbconnect
    def get_last_page_scraped(self, session, brand_id):
        return session.query(Brand.last_page_scraped).filter(Brand.id == brand_id).one()[0]

    def update_last_page_scraped(self, brand_id, website_id, last_page_scraped):
        query = update(Brand).where(Brand.id == brand_id and Brand.website_id == website_id) \
            .values(last_page_scraped=last_page_scraped)
        self.connection.execute(query)

    @dbconnect
    def get_watches_number_for_brand(self, session, brand_id):
        return session.query(Brand.watches_count).filter(Brand.id == brand_id).one()[0]

    @dbconnect
    def get_distinct_watches_number_for_brand(self, session, brand_id):
        return session.query(Watch.item_id).distinct().filter(Watch.brand_id == brand_id).count()

    @dbconnect
    def get_watches_old_number_for_brand(self, session,  brand_id):
        return session.query(Brand.watches_count).filter(Brand.id == brand_id).one()[0]

    @dbconnect
    def _update_watches_old_number_for_brand(self, session, brand_id, watches_count_old):
        query = update(Brand).where(Brand.id == brand_id).values(watches_count_old=watches_count_old)
        self.connection.execute(query)

    @dbconnect
    def update_watches_number_for_brand(self, session, brand_id, watch_count):
        old_watch_count = self.get_watches_old_number_for_brand(brand_id)
        self._update_watches_old_number_for_brand(brand_id, old_watch_count)
        query = update(Brand).where(Brand.id == brand_id).values(watches_count=watch_count)
        self.connection.execute(query)

    @dbconnect
    def get_watches_page_number_for_brand(self, session, brand_id):
        return session.query(Brand.page_count).filter(Brand.id == brand_id).one()[0]

    @dbconnect
    def get_watches_old_page_number_for_brand(self, session, brand_id):
        return session.query(Brand.page_count).filter(Brand.id == brand_id).one()[0]

    def _update_page_old_number_for_brand(self, brand_id, page_count_old):
        query = update(Brand).where(Brand.id == brand_id).values(page_count_old=page_count_old)
        self.connection.execute(query)

    def update_page_number_for_brand(self, brand_id, page_count):
        old_page_count = self.get_watches_old_page_number_for_brand(brand_id)
        self._update_page_old_number_for_brand(brand_id, old_page_count)
        query = update(Brand).where(Brand.id == brand_id).values(page_count=page_count)
        self.connection.execute(query)

    @dbconnect_noexpire
    def insert(self, session, alchemy_object):
        session.add(alchemy_object)
        # object is filled with inserted primary key
        return alchemy_object

    @dbconnect
    def watch_exists(self, session, item_id, website_id):
        return session.query(exists().where(Watch.item_id == item_id and Website.id == website_id)).scalar()

    @dbconnect
    def image_exists(self, sessoin, watch_id, image_url):
        return session.query(exists().where(WatchImage.url == image_url, WatchImage.watch_id)).scalar()

    @dbconnect
    def get_existing_image(self, session, image_url):
        return session.query(WatchImage).filter(WatchImage.url == image_url).first()

    @dbconnect
    def watches_bulk_insert(self, session, watches_list):
        session.bulk_save_objects(watches_list)
        session.commit()

    @dbconnect
    def get_random_watch(self, session, id):
        return session.query(Watch).filter(Watch.id == id).one()

    @dbconnect
    def get_watch(self, session, item_id):
        return session.query(Watch).filter(Watch.item_id == item_id).all()

    @dbconnect
    def get_last_watch(self, session, item_id):
        return session.query(Watch).filter(Watch.item_id == item_id).order_by(Watch.scraped_time.desc()).first()

    @dbconnect
    def get_new_watches(self, session):
        start = datetime.now(self.timezone).replace(hour=0, minute=0, second=1)
        print(start)
        end = start.replace(hour=23, minute=59, second=59)
        #return self.s.query(func.count(Watch.scraped_time > start and Watch.scraped_time < end))
        return session.query(Watch).filter(Watch.scraped_time > start).count()

    @dbconnect
    def get_new_watches_by_brand(self, session, brand_id, day=None, month=None, year=None):
        if all([day, day, year]):
            start = datetime.date(year, day, month)
            start = start.replace(hour=0, minute=0, second=1)
            end = start.replace(hour=23, minute=59, second=59)
        else:
            start = datetime.now(self.timezone).replace(hour=0, minute=0, second=1)
            end = start.replace(hour=23, minute=59, second=59)
        #return self.s.query(func.count(Watch.scraped_time > start and Watch.scraped_time < end))
        return session.query(Watch).filter(and_(Watch.scraped_time > start, Watch.scraped_time < end, Watch.brand_id == brand_id)).count()

    def are_equals(self, base_object, new_object, excluded_fields):
        return super(DaoFactory, self).are_equals(base_object, new_object, excluded_fields)


    def send_email(self, date_extract, file_path, to_list, message, subject):
        emailfrom = 'simon.rochwerg@lobstr.io'
        emailto = to_list
        fileToSend = file_path
        if subject is None:
            subject = "Lobstr.io : extraction {}".format(date_extract)
        username = 'simon.rochwerg@gmail.com'
        password = "AltiuS2010"
        self.mail_sender(emailfrom, emailto, fileToSend, subject, message, username, password)


#mm = DaoFactory("watches")
