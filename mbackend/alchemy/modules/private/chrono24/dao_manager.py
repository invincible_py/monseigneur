from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_brands_to_scrape(self, website_id, max_page_to_scrape):
        return self.daoFactory.get_brands_to_scrape(website_id, max_page_to_scrape)

    def update_last_scraped_time(self, table_name, id):
        return self.daoFactory.update_last_scraped_time(table_name, id)

    def update_last_scraped_time_for_brand(self, brand_id):
        return self.daoFactory.update_last_scraped_time_for_brand(brand_id)

    def get_last_scraped_date(self, brand_id):
        return self.daoFactory.get_last_scraped_date(brand_id)

    def reset_last_page_scraped(self):
        return self.daoFactory.reset_last_page_scraped()

    def get_chrono24_brand_list(self):
        return self.daoFactory.get_chrono24_brand_list()

    def update_last_page_scraped(self, brand_id, last_page_scaped, website_id):
        self.daoFactory.update_last_page_scraped(brand_id, website_id, last_page_scaped)

    def get_last_page_scraped(self, brand_id):
        return self.daoFactory.get_last_page_scraped(brand_id)

    def get_brand_id(self, brand_name):
        return self.daoFactory.get_brand_id(brand_name)

    def get_watches_number_for_brand(self, brand_id):
        return self.daoFactory.get_watches_number_for_brand(brand_id)

    def get_distinct_watches_number_for_brand(self, brand_id):
        return self.daoFactory.get_distinct_watches_number_for_brand(brand_id)

    def get_watches_old_number_for_brand(self, brand_id):
        return self.daoFactory.get_watches_old_number_for_brand(brand_id)

    def update_watches_number_for_brand(self, brand_id, watches_count):
        return self.daoFactory.update_watches_number_for_brand(brand_id, watches_count)

    def update_page_number_for_brand(self, brand_id, page_count):
        return self.daoFactory.update_page_number_for_brand(brand_id, page_count)

    def watches_bulk_insert(self, watches_list):
        self.daoFactory.watches_bulk_insert(watches_list)

    def insert(self, alchemy_object):
        return self.daoFactory.insert(alchemy_object)

    def watch_exists(self, item_id, website_id):
        return self.daoFactory.watch_exists(item_id, website_id)

    def get_existing_image(self, image_url):
        return self.daoFactory.get_existing_image(image_url)

    def image_exists(self, watch_id, image_url):
        return self.daoFactory.image_exists(watch_id, image_url)

    def get_random_watch(self, id):
        return self.daoFactory.get_random_watch(id)

    def get_watch(self, item_id):
        return self.daoFactory.get_watch(item_id)

    def get_last_watch(self, item_id):
        return self.daoFactory.get_last_watch(item_id)

    def get_new_watches(self):
        return self.daoFactory.get_new_watches()

    def get_new_watches_by_brand(self, brand_id, day=None, month=None, year=None):
        return self.daoFactory.get_new_watches_by_brand(brand_id, day=None, month=None, year=None)

    def are_equals(self, base_object, new_object, excluded_fields=[]):
        return self.daoFactory.are_equals(base_object, new_object, excluded_fields)

    def send_email(self, date_extract, file_path, to_list, message, subject):
        return self.daoFactory.send_email(date_extract, file_path, to_list, message, subject)


"""m = DaoManager("watches")
watch1 = m.get_random_watch(74828)
print(watch1.__dict__)
watch2 = m.get_random_watch(83667)
excluded_fields = ["id", "page", "scraped_time"]
print(m.are_equals(watch1, watch2, excluded_fields))
print(m.compar(watch1, watch2))
[[a, b] for a,b in zip(dir(watch1.__dict__.items()) , dir(watch2)) if not a.startswith('__')]

m.update_last_scraped_time("watch", 5)
print(m.get_brand_id("rolex"))
print(m.get_watches_number_for_brand(1))
print(m.get_watches_old_number_for_brand(1))
#print(m.update_watches_number_for_brand(1, 69700))
print(m.update_page_number_for_brand(1, 400))

print("compareee")
print(m.are_equals(watch1, watch2, ["id", "page", "scraped_time"]))"""
