from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT
from sqlalchemy.types import DateTime


Base = declarative_base()


class Website(Base):
    __tablename__ = 'website'

    id = Column(Integer, primary_key=True)
    name = Column(TINYTEXT)
    url = Column(TINYTEXT)


class BrandName(Base):
    __tablename__ = "brand_name"

    id = Column(Integer, primary_key=True)
    brand_name = Column(TINYTEXT)


class Brand(Base):
    __tablename__ = "watch_brand"

    id = Column(Integer, primary_key=True)
    brand_id = Column(Integer, ForeignKey('brand_name.id'))
    website_id = Column(Integer, ForeignKey('website.id'))
    website = relationship(Website, backref=backref('website', uselist=True))
    brand_link = Column(TINYTEXT)
    watches_count = Column(Integer)
    watches_count_old = Column(Integer)
    page_count = Column(Integer)
    page_count_old = Column(Integer)
    last_page_scraped = Column(Integer)
    last_scraped_time = Column(DateTime)


class Watch(Base):
    __tablename__ = 'watch'

    id = Column(Integer, primary_key=True)
    image = relationship("Association")
    brand_id = Column(Integer, ForeignKey('watch_brand.id'))

    #brand = relationship(Brand, backref=backref('brand', uselist=True))
    page = Column(Integer)
    scraped_time = Column(DateTime)
    item_id = Column(Integer)
    link = Column(String(1000))
    title = Column(MEDIUMTEXT)
    first_price = Column(Integer)
    price_description = Column(TINYTEXT)
    offer_number = Column(TINYTEXT)
    currency = Column(TINYTEXT)
    state = Column(TINYTEXT)
    delivered_content = Column(TINYTEXT)
    availability_0 = Column(TINYTEXT)
    description = Column(MEDIUMTEXT)
    original_description = Column(MEDIUMTEXT)
    delivery_delay = Column(TINYTEXT)
    delivery_cost = Column(TINYTEXT)
    reference_number = Column(TINYTEXT)
    code = Column(TINYTEXT)
    brand = Column(TINYTEXT)
    model = Column(TINYTEXT)
    movement = Column(TINYTEXT)
    caliber_gear = Column(TINYTEXT)
    power_reserve = Column(TINYTEXT)
    diameter = Column(TINYTEXT)
    watertight = Column(TINYTEXT)
    binocular_material = Column(TINYTEXT)
    glass = Column(TINYTEXT)
    dial = Column(TINYTEXT)
    dial_figures = Column(TINYTEXT)
    box = Column(TINYTEXT)
    wristlet_material = Column(TINYTEXT)
    wristlet_color = Column(TINYTEXT)
    wristlet_loop = Column(TINYTEXT)
    locking = Column(TINYTEXT)
    year = Column(TINYTEXT)
    state_details = Column(TINYTEXT)
    complications = Column(TINYTEXT)
    others = Column(MEDIUMTEXT)
    sex = Column(TINYTEXT)
    place = Column(TINYTEXT)
    price = Column(TINYTEXT)
    availability = Column(TINYTEXT)
    seller_address = Column(TINYTEXT)
    phone_number = Column(TINYTEXT)
    fax = Column(TINYTEXT)
    benefits = Column(TINYTEXT)
    seller_name = Column(TINYTEXT)
    seller_trusted = Column(TINYTEXT)
    seller_watches = Column(TINYTEXT)
    seller_rating = Column(TINYTEXT)
    seller_trusted_checkout = Column(TINYTEXT)
    seller_expedition_mark = Column(Integer)
    seller_description_mark = Column(Integer)
    seller_communication_mark = Column(Integer)
    pro_seller_location = Column(TINYTEXT)
    seller_nationality = Column(TINYTEXT)

    # ------------------invaluables fields----------------------

class HistoryWatch():
    __tablename__ = 'invaluables'

    id = Column(Integer, primary_key=True)
    key = Column(String(1000))
    url = Column(String(1000))
    title = Column(TINYTEXT)
    brand = Column(TINYTEXT)
    auction_house = Column(TINYTEXT)
    auction_date = Column(TINYTEXT)
    auction_address = Column(TINYTEXT)
    auction_collection = Column(TINYTEXT)
    estimate = Column(TINYTEXT)
    realized = Column(TINYTEXT)
    currency = Column(TINYTEXT)
    saved_count = Column(TINYTEXT)
    bids = Column(TINYTEXT)
    description = Column(TINYTEXT)
    condition = Column(TINYTEXT)
    page = Column(TINYTEXT)
    image = Column(TINYTEXT)
    logged = Column(TINYTEXT)
    date_range = Column(TINYTEXT)
    scrapped_time = Column(TINYTEXT)

class WatchImage(Base):
    __tablename__ = "watch_image"
    id = Column(Integer, primary_key=True)
    scraped_time = Column(DateTime)
    url = Column(String(1000))
    path = Column(String(1000))


class Association(Base):
    __tablename__ = 'watch_image_association'
    watch_id = Column(Integer, ForeignKey('watch.id'), primary_key=True)
    image_id = Column(Integer, ForeignKey('watch_image.id'), primary_key=True)
    image = relationship("WatchImage")


class Monitoring(Base):
    __tablename__ = 'monitoring'
    id = Column(Integer, primary_key=True)
    last_watch_sent_id = Column(Integer)
    sent_time = Column(DateTime)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)


