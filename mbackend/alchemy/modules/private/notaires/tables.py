from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.types import String, Float, REAL


class Base(object):
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    __mysql_charset = 'utf8mb4'


Base = declarative_base(Base)


class Notaires(Base):
    __tablename__ = "notaires"

    ID = Column(Integer, primary_key=True, autoincrement=True)
    NAME = Column(String(length=500))
    ADRESS = Column(String(length=500))
    CP = Column(String(length=500))
    CITY = Column(String(length=500))
    PHONE = Column(String(length=500))
    FAX = Column(String(length=500))
    WEB = Column(String(length=500))
    MAIL = Column(String(length=500))
    URL = Column(String(length=500), unique=True)
    LAT = Column(REAL)
    LNG = Column(REAL)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)