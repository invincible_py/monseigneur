from alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import Base, Monitoring, MonitoringAnnonce

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy.sql.expression import exists
from sqlalchemy import or_
from sqlalchemy.orm.exc import NoResultFound
from datetime import timedelta, datetime, date


class DaoFactory(DaoFactory):

    HOURS_INTERVAL_TO_SCRAPE = 12

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        Base.create_all(self.engine)

    def select_monitoring(self, session):
        xhoursago = datetime.now(self.timezone) - timedelta(hours=12)
        return session.query(Monitoring).filter(Monitoring.scraping_time < xhoursago).all()

    def select_annonces_monitoring(self, session):
        return session.query(MonitoringAnnonce).all()
