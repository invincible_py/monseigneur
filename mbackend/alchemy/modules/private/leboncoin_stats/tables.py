from sqlalchemy import Column, ForeignKey
from mbackend.alchemy.core.base import Base
from sqlalchemy.types import Integer, DateTime, Float

from sqlalchemy.dialects.mysql import VARCHAR, JSON
from sqlalchemy.orm import relationship


class Monitoring(Base):
    __tablename__ = 'monitoring_statsrecord'

    category = Column(VARCHAR(200))
    category_slug = Column(VARCHAR(200))
    region = Column(VARCHAR(200))
    region_slug = Column(VARCHAR(200))


class StatsRecord(Base):


    total_pro = Column(Integer)
    total_private = Column(Integer)
    total = Column(Integer)
    url = Column(VARCHAR(800))

    monitoring = relationship("Monitoring")
    monitoring_id = Column(Integer, ForeignKey('monitoring_statsrecord.id'))


class MonitoringAnnonce(Base):
    __tablename__ = 'monitoring_annonce'

    category = Column(VARCHAR(200))
    category_slug = Column(VARCHAR(200))
    region = Column(VARCHAR(200))
    region_slug = Column(VARCHAR(200))
    last_page_scraped = Column(Integer)
    last_annonce_date = Column(DateTime)
    total_pages = Column(Integer)

    annonces = relationship("Annonce")


class Annonce(Base):

    annonce_id = Column(Integer, unique=True)
    expiration_date = Column(DateTime)
    owner = Column(JSON)
    ad_type = Column(VARCHAR(50))
    owner_type = Column(VARCHAR(30))
    url = Column(VARCHAR(200))
    category_name = Column(VARCHAR(50))
    city = Column(VARCHAR(100))
    postal_code = Column(Integer)
    price = Column(Float)
    lng = Column(Float)
    lat = Column(Float)
    first_publication_date = Column(DateTime)
    last_publication_date = Column(DateTime)

    monitoring_id = Column(Integer, ForeignKey('monitoring_annonce.id'))
