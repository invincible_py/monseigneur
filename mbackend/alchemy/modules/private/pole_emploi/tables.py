from sqlalchemy import Column, String, Integer, ForeignKey, Table, Float
from mbackend.alchemy.core.base import Base
from mbackend.alchemy.objects.real_estate.annonce import Annonce


from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, DECIMAL, BOOLEAN
from sqlalchemy.types import DateTime, Float, String
from sqlalchemy.orm import relationship

import datetime


class Profile(Base):

    __tablename__ = 'profile'

    id = Column(Integer, autoincrement=True, primary_key=True)
    id_candidat = Column(VARCHAR(250), unique=True, primary_key=True)

    nom = Column(VARCHAR(250))
    titre = Column(MEDIUMTEXT)
    code_postal = Column(Integer)
    code_job = Column(Integer)
    code_competence = Column(Integer)
    maj = Column(DateTime)
    telephone = Column(VARCHAR(250))
    mail = Column(VARCHAR(250))

class Monitoring(Base):
    __tablename__ = 'monitoring'

    id = Column(Integer, autoincrement=True, primary_key=True)

    code_postal = Column(Integer)
    code_job = Column(Integer)
    code_competence = Column(Integer)

    total_results = Column(Integer)
    is_done = Column(BOOLEAN, default=False)

def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
