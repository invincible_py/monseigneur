from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def count_by_unique_id(self, _id):
        return self.daoFactory.count_by_unique_id(_id)

    def insert(self, _object):
        return self.daoFactory.insert(_object)

    def count_monitoring_exist(self, postal_code, job_code, skill_code):
        return self.daoFactory.count_monitoring_exist(postal_code, job_code, skill_code)

    def count_monitoring_is_done(self, postal_code, job_code, skill_code):
        return self.daoFactory.count_monitoring_is_done(postal_code, job_code, skill_code)

    def update_monitoring_is_done(self, postal_code, job_code, skill_code, total_results):
        return self.daoFactory.update_monitoring_is_done(postal_code, job_code, skill_code, total_results)

    def count_total_results(self, postal_code, job_code, skill_code):
        return self.daoFactory.count_total_results(postal_code, job_code, skill_code)
