from sqlalchemy import Column, Integer, DECIMAL

from sqlalchemy.dialects.mysql import VARCHAR

from mbackend.alchemy.core.base import Base, DateTime
from sqlalchemy.types import Date


class Account(Base):
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True)
    username = Column(VARCHAR(length=100))
    password = Column(VARCHAR(length=100))
    scraping_time = Column(DateTime, nullable=True)


class Monitoring(Base):
    __tablename__ = 'monitoring'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    price_date = Column(Date)
    SP95E10 = Column(DECIMAL(10, 3))
    SP98 = Column(DECIMAL(10, 3))
    GO_PLUS = Column(DECIMAL(10, 3))
    GAZOLE = Column(DECIMAL(10, 3))


class MonitoringUpdates(Base):
    __tablename__ = 'monitoring_updates'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    gasole = Column(DECIMAL(10, 3))
    sp95e10 = Column(DECIMAL(10, 3))
    sp98 = Column(DECIMAL(10, 3))


def create_all(engine):
    print("Creating databases")
    Base.metadata.create_all(engine)
