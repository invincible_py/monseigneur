from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noclosingandsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from .tables import *


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect
    def insert(self, session, instance_object):
        session.add(instance_object)

    @dbconnect
    def accounts_count(self, session, alchemy_object):
        return session.query(alchemy_object).count()

    @dbconnect
    def select_account_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object.username, alchemy_object.password).filter(alchemy_object.id == id).first()

    @dbconnect
    def get_last_prices(self, session):
        return session.query(Monitoring.GAZOLE, Monitoring.SP95E10, Monitoring.SP98).order_by(Monitoring.id.desc()).first()
