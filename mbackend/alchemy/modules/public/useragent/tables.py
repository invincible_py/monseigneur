from sqlalchemy import Column, Integer
from sqlalchemy.orm import relationship
from mbackend.alchemy.core.base import Base

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, TEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, FLOAT


class UserAgent(Base):

    percent = Column(FLOAT)
    useragent = Column(VARCHAR(200))
    system = Column(VARCHAR(100))


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
