from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy import Column, Integer
from sqlalchemy.types import DateTime


class Monitoring(object):
    __tablename__ = 'monitoring'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    __mysql_charset = 'utf8mb4'

    id = Column(Integer, primary_key=True)
    total_annonces = Column(Integer)
    last_page_scraped = Column(Integer)
    last_publication_date = Column(DateTime)
    last_scraping_date = Column(DateTime)
    url = Column(MEDIUMTEXT)
    search_slug = Column(MEDIUMTEXT)
    last_url_scraped = Column(MEDIUMTEXT)
