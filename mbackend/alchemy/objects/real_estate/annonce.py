from sqlalchemy import Column, Integer, ForeignKey

from sqlalchemy.dialects.mysql import LONGTEXT, MEDIUMTEXT, VARCHAR, BOOLEAN, JSON, DECIMAL, FLOAT
from sqlalchemy.types import DateTime, Float
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr


class Annonce(object):
    __tablename__ = 'annonce'

    id = Column(Integer, primary_key=True, autoincrement=True)

    annonce_id = Column(VARCHAR(200), unique=True)
    ref = Column(VARCHAR(250))
    is_exclusive = Column(BOOLEAN)

    url = Column(VARCHAR(800), unique=True)

    title = Column(MEDIUMTEXT)

    GES_string = Column(VARCHAR(100))
    GES_int = Column(Integer)
    GES = Column(VARCHAR(1))

    DPE_string = Column(VARCHAR(100))
    DPE_int = Column(Integer)
    DPE = Column(VARCHAR(1))

    room_count = Column(Integer)
    sleepingroom_count = Column(Integer)

    charges_included = Column(BOOLEAN)
    area = Column(Integer, index=True)

    floor = Column(Integer)

    price = Column(Integer, index=True)
    square_metter_price = Column(Float)

    currency = Column(VARCHAR(10), index=True)
    city = Column(VARCHAR(200))
    postal_code = Column(DECIMAL(8, 0), index=True)
    furnished = Column(BOOLEAN)

    district = Column(VARCHAR(300))

    description = Column(LONGTEXT)
    main_picture = Column(VARCHAR(300))

    #seloger fields

    heattype = Column(VARCHAR(150), index=True)
    kitchentype = Column(VARCHAR(150), index=True)

    balcony = Column(BOOLEAN, index=True)
    bathroom_count = Column(BOOLEAN, index=True)
    laundry_room = Column(BOOLEAN, index=True)

    monthly_financial_fee = Column(Float)



    details = Column(JSON)

    lat = Column(FLOAT)
    lng = Column(FLOAT)

    first_publication_date = Column(DateTime, index=True)

    first_scraping_date = Column(DateTime, index=True)
    last_scraping_date = Column(DateTime, index=True)
    scraping_date = Column(DateTime, index=True)

    expiration_date = Column(DateTime, index=True)

    pseudo = Column(VARCHAR(30))

    has_phone = Column(BOOLEAN, index=True)

    page_scraped = Column(Integer)

    @declared_attr
    def real_estate_type_id(cls):
        return Column(Integer, ForeignKey('real_estate_type.id'))

    @declared_attr
    def real_estate_type(cls):
        return relationship("RealEstateType", cascade="save-update")

    @declared_attr
    def monitoring_id(cls):
        return Column(Integer, ForeignKey('monitoring.id'))

    @declared_attr
    def monitoring(cls):
        return relationship("Monitoring", cascade="save-update")

    @declared_attr
    def owner(cls):
        return relationship('Owner', cascade="save-update")

    @declared_attr
    def owner_id(cls):
        return Column(Integer, ForeignKey('owner.id'))

    @declared_attr
    def pictures(cls):
        return relationship("Picture", cascade="save-update")

    @declared_attr
    def category(cls):
        return relationship('Category', cascade="save-update")

    @declared_attr
    def category_id(cls):
        return Column(Integer, ForeignKey('category.id'))

    @declared_attr
    def region(cls):
        return relationship('Region', cascade="save-update")

    @declared_attr
    def region_id(cls):
        return Column(Integer, ForeignKey('region.id'))

    @declared_attr
    def department(cls):
        return relationship('Department', cascade="save-update")

    @declared_attr
    def department_id(cls):
        return Column(Integer, ForeignKey('department.id'))

    @declared_attr
    def phone_id(cls):
        return Column(Integer, ForeignKey('phone.id'))

    @declared_attr
    def phone(cls):
        return relationship("Phone", cascade="save-update")


class RealEstateType(object):
    """
    Appartement, Maison etc
    """
    __tablename__ = 'real_estate_type'

    id = Column(Integer, primary_key=True, autoincrement=True)
    real_estate_type_id = Column(Integer, unique=True)
    name = Column(VARCHAR(200), unique=True, index=True)


class Owner(object):
    """
    Particulier/Professionnel/Name/etc
    """
    __tablename__ = 'owner'

    id = Column(Integer, primary_key=True, autoincrement=True)
    store_id = Column(Integer, index=True)
    user_id = Column(VARCHAR(200), index=True, unique=True)
    pro_rates_link = Column(VARCHAR(500))

    detail_link = Column(VARCHAR(200))

    # owner_type = 0: particulier, 1: professional
    owner_type = Column(BOOLEAN, index=True)
    name = Column(VARCHAR(200), index=True)
    siren = Column(Integer, index=True)

    description = Column(MEDIUMTEXT)
    address = Column(MEDIUMTEXT)

    phone = Column(MEDIUMTEXT)


class Picture(object):
    __tablename__ = "picture"

    id = Column(Integer, primary_key=True, autoincrement=True)
    url = Column(VARCHAR(800))
    position = Column(Integer, index=True)
    scraping_date = Column(DateTime)

    @declared_attr
    def annonce_id(cls):
        return Column(Integer, ForeignKey('annonce.id'))


class Category(object):
    """
    Location, Ventes, Etc
    """
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True, autoincrement=True)
    category_id = Column(Integer, unique=True)
    name = Column(VARCHAR(200), unique=True)


class Region(object):
    __tablename__ = 'region'

    id = Column(Integer, primary_key=True, autoincrement=True)
    region_id = Column(Integer, unique=True)
    name = Column(VARCHAR(200), unique=True)


class Department(object):
    __tablename__ = 'department'

    id = Column(Integer, primary_key=True, autoincrement=True)
    department_id = Column(Integer, unique=True)
    name = Column(VARCHAR(200), unique=True)


class Phone(object):
    __tablename__ = 'phone'

    id = Column(Integer, primary_key=True)
    phone = Column(VARCHAR(15), index=True)
