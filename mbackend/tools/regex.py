import re


class Regex(object):

    mail = re.compile(r"[\w\.\-]{2,100}\@[\w\-]{2,100}\.[a-z]{2,100}\.*[a-z]*(?=\s|$|\"|\<|\?|\>|\(|\))")

    # FR
    phone = re.compile(r"(?<=\s|\"|<|\?|>|\(|\)|:)(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})(?=\s|$|\"|<|\?|>|\(|\))")
    personal_phone = re.compile(r"(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[6-7]")
    # https://stackoverflow.com/questions/38483885/regex-for-french-telephone-numbers

    instagram = re.compile(r"(?<=href=\")https\:\/\/www\.instagram\.com\/[^\"]+")
    facebook = re.compile(r"(?<=href=\")https\:\/\/(?:.*\.)?facebook\.com\/[^\"]+")
    twitter = re.compile(r"(?<=href=\")https://(?:www\.)?twitter\.com/[^\"]+")
    linkedin = re.compile(r"(?<=href=\")https\:\/\/www\.linkedin\.com\/(?:company|in)\/[^\"]+")
    youtube = re.compile(r"(?<=href=\")https\:\/\/www\.youtube\.com\/c(?:hannel)?\/[^\"]+")
    viadeo = re.compile(r"(?<=href=\")http(?:s)?\:\/\/(?:.*\.)?viadeo\.com\/.*\/company\/[^\"]+")

    price = re.compile(r"\d{1,3}(?:[.,\s]\d{3})*(?:[.,]\d{2})?(?=\s?\€|\$|\£)")
    siret = re.compile(r"(?<=[^0-9]{1})\d{3}\s?\d{3}\s?\d{3}\s?\d{5}(?=[^0-9]{1})")

    def __repr__(self):
        return Regex
