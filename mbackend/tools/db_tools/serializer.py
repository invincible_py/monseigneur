from sqlalchemy.ext.declarative import DeclarativeMeta
import json
import datetime
from decimal import Decimal


class MyAlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                if isinstance(data, datetime.date):
                    data = str(data)
                if isinstance(data, Decimal):
                    data = float(data)
                json.dumps(data)
                fields[field] = data
            # a json-encodable dict
            return fields
        return json.JSONEncoder.default(self, obj)


def alchemy_to_json(obj):
    return json.dumps(obj, cls=json.JSONEncoder)
