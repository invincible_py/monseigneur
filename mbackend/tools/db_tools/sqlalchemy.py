import collections.abc

def _is_sa_mapped(cls):
    return hasattr(cls, '__table__')


def update_object(destination_object, source_object, excluded_fields=[]):
    for k_d1, v_d1 in source_object.__dict__.items():
        if k_d1 not in excluded_fields and k_d1 != "_sa_instance_state" and v_d1 != None:
            setattr(destination_object, k_d1, v_d1)
    return destination_object


def update_nested_object(destination_object, source_object, excluded_fields=[]):
    for dico_object in [source_object]:
        for k, v in dico_object.__dict__.items():
            if isinstance(v, str):
                v = v.replace("\x00", "").replace("0x00", "").encode("utf-8").decode("utf-8", errors="ignore").replace("\u0000", "").replace("u0000", "")
                setattr(destination_object, k, v)
            # v != None -> if the attributes is null, we avoid to erase an existing attribute in the database
            if k not in excluded_fields and k != "_sa_instance_state" and v is not None:
                if isinstance(v, collections.abc.Mapping) and _is_sa_mapped(v):
                    # recursivity
                    setattr(destination_object, k, update_nested_object(getattr(destination_object, k, {}), v), excluded_fields=excluded_fields)
                elif isinstance(v, list):
                    for s in v:
                        if isinstance(v, collections.abc.Mapping) and _is_sa_mapped(v):
                            setattr(destination_object, k, update_nested_object(getattr(destination_object, k, {}), v))
                else:
                    setattr(destination_object, k,  v)
    return destination_object


def clean_nested_object(source_object):
    # check if the object is a sqlalchemy object
    if _is_sa_mapped(source_object):
        for k, v in source_object.__dict__.items():
            if isinstance(v, collections.abc.Mapping):
                clean_nested_object(v)
            elif isinstance(v, list):
                for s in v:
                    clean_nested_object(s)
            elif isinstance(v, str):
                cleaned_string = v.replace("\x00", "").replace("0x00", "").encode("utf-8").decode("utf-8", errors="ignore").replace("\\u0000", "").replace("u0000", "")
                setattr(source_object, k, cleaned_string)
            elif _is_sa_mapped(v):
                clean_nested_object(v)
    return source_object