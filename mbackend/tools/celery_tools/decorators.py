# -*- coding: utf-8 -*-
import coloredlogs, logging
coloredlogs.install()


def log_and_kill(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            import os
            import traceback
            logging.warning(traceback.format_exc())
            os.system("pkill -9 -f 'worker_task worker'")
    return wrapper

