from unidecode import unidecode
import difflib


class Matching(object):

    def __init__(self):
        super(Matching, self).__init__()

    def __repr__(self):
        return Matching.__dict__

    def text_similarity(self, str_a, str_b):

        assert isinstance(str_a, str)
        assert isinstance(str_b, str)

        str_a_normalized = unidecode(str_a.lower()).strip(' ')
        str_b_normalized = unidecode(str_b.lower()).strip(' ')

        seq = difflib.SequenceMatcher(None, str_a_normalized, str_b_normalized)

        return round(seq.ratio(), 2)
