class GeocodeGenerator(object):

    """Generate list of geocodes from x,y geocodes"""

    def __init__(self, _list, total):
        super(GeocodeGenerator, self).__init__()

        self._list = _list

        assert len(self._list) == 2
        for g in self._list:
            assert isinstance(g, tuple)
            for p in g:
                assert isinstance(p, float)

        assert self._list[0] is not self._list[1]

        self.total = total
        self.root = int(total**(1/2))
        assert self.root**2 == self.total

        self.x = self._list[0]
        self.y = self._list[1]

        assert self.x[0] != self.y[0]
        assert self.x[1] != self.y[1]

        print(self.x)
        print(self.y)

    def __repr__(self):
        return GeocodeGenerator.__dict__

    def __call__(self):
        x_distance = max([self.y[0], self.x[0]]) - min([self.y[0], self.x[0]])
        y_distance = max([self.y[1], self.x[1]]) - min([self.y[1], self.x[1]])

        geo_start = (min(self.x[0], self.y[0]), min(self.x[1], self.y[1]))

        step = int(self.total**(1/2))  # square
        x_step = x_distance / step
        y_step = y_distance / step

        _list = [(geo_start[0] + step_1*x_step, geo_start[1] + step_2*y_step) for step_1 in range(step) for step_2 in range(step)]

        return _list
