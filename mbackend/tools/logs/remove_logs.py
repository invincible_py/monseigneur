import os

from apscheduler.schedulers.background import BlockingScheduler
from apscheduler.executors.pool import ThreadPoolExecutor
from pytz import timezone
import time
from datetime import datetime, timedelta
import shutil


class LogRemover():

    def __init__(self):
        self.timezone = timezone('Europe/Paris')
        self.datadir = "/home/simon/app_logs/saved_responses"

    def interval_jobs(self):
        self.scheduler = BlockingScheduler(executors={'default': ThreadPoolExecutor(1)}, timezone=timezone('Europe/Paris'))

        paris = timezone('Europe/Paris')
        now = datetime.now(paris) + timedelta(seconds=3)

        self.scheduler.add_job(self.remove_useless_logs, "interval", id='remove_useless',  minutes=30, start_date=now)
        self.scheduler.add_job(self.remove_all_logs, "interval", id='remove_all', minutes=60, start_date=now, misfire_grace_time=3600)
        self.scheduler.start()

    def remove_useless_logs(self):
        try:
            for root, dirs, files in os.walk(self.datadir):
                for name in files:
                    self.current_name = name
                    if name != "url_response_match.txt":
                        try:
                            code = int(name.split("-")[1])
                        except ValueError:
                            code = int(name.split("-")[1].split(".")[0])
                        if code == 200:
                            os.remove(os.path.join(root, name))
                    else:
                        os.remove(os.path.join(root, name))

        except Exception as e:
            print(self.current_name)
            print(e)
        finally:
            print("useless logs cleanup done")

    def remove_all_logs(self):
        paris = timezone('Europe/Paris')
        old = datetime.now(paris) - timedelta(hours=1)
        try:
            for root, dirs, files in os.walk(self.datadir):
                for name in files:
                    os.remove(os.path.join(root, name))
            for root, dirs, files in os.walk(self.datadir):
                for dir in dirs:
                    for subroot, subdir, subfiles in os.walk(dir):
                        if time.ctime(os.path.getmtime(subdir)) < old:
                            if not subfiles:
                                shutil.rmtree(os.path.join(root, subroot))

        except Exception as e:
            print(e)
        finally:
            print("all logs cleanup done")


launcher = LogRemover()
launcher.interval_jobs()
