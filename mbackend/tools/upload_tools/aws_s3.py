import logging
import boto3
from botocore.exceptions import ClientError

# sudo apt install awscli
# aws configure
# create key : https://console.aws.amazon.com/iam/home#/users/User_Name?section=security_credentials


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :type str
    :param bucket: Bucket to upload to
    :type str
    :param object_name: S3 object name. If not specified then file_name is used
    :type str

    :return: True if file was uploaded, else False

    >>> s3 = boto3.resource('s3')
    >>> s3.meta.client.upload_file('/tmp/hello.txt', 'mybucket', 'hello.txt')

    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True
