from monseigneur.modules.factfarma.core.backend import PharmaBackend


class {{cookiecutter.camel_name}}Backend(PharmaBackend):
    APPNAME = "Application {{cookiecutter.camel_name}}"
    VERSION = "1.0"

    def __init__(self):
        self.module_name = '{{cookiecutter.directory_name}}'
        PharmaBackend.__init__(self)
        # self.unique_product_attribute = ["url", "internal_code", "ean13", "ean7"]
        # self.unique_child_product_attribute = "variation"
        # self.history_attributes = []
        self.setup_logging()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--db")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    crawler = {{cookiecutter.camel_name}}Backend()
    crawler.set_engine_config(args.db, args.test)
    # crawler.insert_template()
    # crawler.insert_subdomain()
    crawler.insert_update_meta()
    crawler.insert_update_categories()
