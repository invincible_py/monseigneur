from sqlalchemy_utils import create_database, database_exists
from sqlalchemy import create_engine, exists
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import text
from monseigneur.core.tools.log import getLogger
import os
import json
import csv
import datetime
from pytz import timezone
import schedule
import argparse
import time
import logging

from mbackend.tools.email_tools.mailgun_file_sender import send_complex_message

logging.basicConfig(
    filemode='a',
    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
    datefmt='%H:%M:%S',
    level=logging.DEBUG
)


class Engine:

    try:
        DATA_PATH = os.environ['DATAPATH']
    except KeyError:
        DATA_PATH = '{}/{}'.format(os.environ['HOME'], 'mdev/monseigneur/mbackend')

    def __init__(self, database_name):

        self.logger = getLogger("dao_logger")
        self.config = None
        self.sql_username = None
        self.sql_password = None
        self.engine_config = None
        self.engine = None
        self.connection = None
        self.session_factory = None
        self.database_name = database_name

    def extract_query(self, _file_path, _data, _header, separator):
        with open(_file_path, 'w') as csvfile:
            outcsv = csv.writer(csvfile, delimiter=separator, quotechar='"', quoting=csv.QUOTE_MINIMAL)
            outcsv.writerow(_header)
            for record in _data:
                outcsv.writerow([getattr(record, c) for c in _header])
        return True

    def create_sql_connection(self):
        with open(os.path.join(self.DATA_PATH, 'config.json')) as json_data_file:
            self.config = json.load(json_data_file)

        self.sql_username = self.config["engine_config"]["sql_username"]
        self.sql_password = self.config["engine_config"]["sql_password"]

        self.engine_config = 'mysql://{0}:{1}@localhost/{2}?charset=utf8mb4'.format(
            self.sql_username,
            self.sql_password,
            self.database_name
        )

        self.engine = create_engine(self.engine_config)
        if not database_exists(self.engine.url):
            create_database(self.engine.url)
        self.connection = self.engine.connect()

        self.session_factory = sessionmaker(bind=self.engine)

    def job(self, dir_path, client_mail, client_name, module, header, query, separator):
        self.logger.info('Starting job')
        self.create_sql_connection()
        current_date = datetime.datetime.now(tz=timezone('Europe/Paris'))
        yesterday_date = current_date + datetime.timedelta(days=-1)
        file_name = '{0}_{1}_{2}.csv'.format(module, yesterday_date.strftime('%Y%m%d'), client_name)
        file_path = '/'.join([dir_path, file_name])
        query = text("""{}""".format(query))
        data = self.engine.execute(query)
        query_result = self.extract_query(_file_path=file_path, _data=data, _header=header, separator=separator)
        assert query_result
        self.logger.info('File extracted')

        mail_response = send_complex_message(
            to_recipients_list=client_mail,
            file_name=file_name,
            file_path=file_path,
            module=module
        )
        assert mail_response.status_code == 200
        self.logger.info('Job done')


def main():
    # TODO Paste args or params from auth_db

    parser = argparse.ArgumentParser(description='leboncoin sender')
    parser.add_argument('--email', type=str, help='email of user')
    parser.add_argument('--client_name', type=str, help='client_name')
    parser.add_argument('--hour', type=str, help='hour to send mail')
    parser.add_argument('--query', type=str, help='query')
    parser.add_argument('--day_before', type=int, help='day_before')

    args = parser.parse_args()

    dir_path = "/mnt/data/leboncoin_files"
    client_mail = args.email
    client_name = args.client_name
    hour = args.hour
    query = args.query

    day_before = args.day_before

    module = "leboncoin"
    engine = Engine("leboncoin_immo")
    current_date = datetime.datetime.now(tz=timezone('Europe/Paris'))
    day_before_date = current_date + datetime.timedelta(days=-day_before)

    # .format(date=yesterday_date.strftime('%Y-%m-%d %H:%M:%S.%f')
    query += ' and scraping_date >= "{date}"'.format(date=day_before_date.strftime('%Y-%m-%d %H:%M:%S.%f'))

    header = ["id", "annonce_id", "title", "url", "photos", "pseudo", "GES", "DPE",
              "room", "utilities", "area", "cost", "currency", "text", "city", "postal_code",
              "lat", "lng", "department_name", "department_id", "region", "advert_type",
              "price_per_meter", "first_publication_date", "furnished", "type", "sales_type",
              "scraping_date", "page_scraped", "api_key", "phone", "has_phone", "urgent",
              "user_id", "phone_from_user", "type_bien"]
    separator = ";"
    # query.format(yesterday_date=yesterday_date)
    # schedule.every().day.at(hour).do(engine.job, dir_path=dir_path, client_mail=client_mail, module=module, header=header, query=query)
    # schedule.every(1).minutes.do(engine.job, dir_path=dir_path, client_mail=client_mail, module=module, header=header, query=query)
    # schedule.every(2).minutes.do(engine.job, dir_path=dir_path, client_mail=client_mail, module=module)
    # engine.logger.info('job scheduled')
    schedule.every().day.at(hour).do(engine.job, dir_path=dir_path, client_mail=client_mail, client_name=client_name, module=module, header=header, query=query, separator=separator)

    while True:
        schedule.run_pending()
        time.sleep(1)


main()

# example python3 leboncoin_sender.py --email patrick.gihan@wanadoo.fr --client_name patrick_gihan --hour 17:20 --day_before 1 --query 'select * from annonces where type_bien = "House" and department_name = "Hérault" '
