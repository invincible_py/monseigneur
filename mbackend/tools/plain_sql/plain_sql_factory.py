import mysql.connector
import re
from pytz import timezone


class PlainSqlFactory:

    def __init__(self, host, database_name, username, password):
        self.config = {
            'user': username,
            'password': password,
            'host': host,
            'database': database_name,
            'raise_on_warnings': True,
            'charset' : 'utf8'
         }
        self.timezone = timezone('Europe/Paris')

    @classmethod
    def getInstance(cls,table_name):
        new_instance = cls(table_name)
        return new_instance

    def getCursor(self):
        self.connection = mysql.connector.connect(**self.config)
        self.cursor = self.connection.cursor()
        return self.cursor

    def closeConnection(self):
        self.cursor.close()
        self.connection.close()

    def _ins_query_maker(self, tableName, rowDict):
        columnList = []
        columnValues = []
        for field, value in zip(rowDict.keys(), rowDict.values()):
            columnList.append("`" + field + "`")
            columnValues.append("'" + re.escape(str(value)) + "'")
        finalQuery = "INSERT INTO " + "`" + tableName + "`" + " (" + ",".join(
            columnList) + ")" + " VALUES (" + ",".join(columnValues) + ")"
        return finalQuery

    def execute_query(self, query):
        self.getCursor()
        self.cursor.execute(query)
        responses = []

        for response in self.cursor:
            responses.append(response)

        self.connection.commit()
        self.closeConnection()
        return responses

    def insertInTable(self,tablename, rowdict):
        query = self._ins_query_maker(tablename, rowdict)
        self.getCursor()
        self.cursor.execute(query)
        self.connection.commit()
        self.closeConnection()

    def updateInTable(self,tableName, rowDict,criteriaValue, id):
        query = self._update_query_maker(tableName, rowDict,criteriaValue, id)
        self.getCursor()
        self.cursor.execute(query)
        self.connection.commit()
        self.closeConnection()

    def _rowExists(self,tableName,countCriteria,countCriteriaValue):
        query = self._exists_query_maker(tableName,countCriteria, countCriteriaValue)
        self.getCursor()
        self.cursor.execute(query)
        response = None
        for count in self.cursor:
            if count[0] ==0:
                response = False
            else:
                response = True
        self.closeConnection()
        return response

    def _exists_query_maker(self, tableName, countCriteria, countCriteriaValue):
        query = "SELECT COUNT(*) AS count FROM " +  tableName + " WHERE " + countCriteria + " = " + \
                "'" + re.escape(str(countCriteriaValue)) + "';"
        return query

    def _update_query_maker(self, tableName, rowDict,criteriaValue, id):
        itemList = []
        for field, value in zip(rowDict.keys(),rowDict.values()):
            itemList.append(field + "=" + "\'" + str(value) + "\'")

        finalQuery  = "UPDATE " + tableName + " SET " + ",".join(itemList) + " WHERE " + str(criteriaValue) + " = " +  \
                      "\'" + str(id) +"\'"
        return finalQuery
