# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

__all__ = ['CaptchaError', 'NoCardsError', 'RedirectionError']


class CaptchaError(Exception):
    pass


class NoCardsError(Exception):
    pass


class RedirectionError(Exception):
    pass


class ExpiredError(Exception):
    pass

class PhoneDesactivated(Exception):
    pass
