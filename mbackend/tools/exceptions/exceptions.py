class JsonNotThere(Exception):
    pass


class JsonIncomplete(Exception):
    pass


class EmptyPage(Exception):
    pass


class CaptchaException(Exception):
    pass


class TooMuchCaptchaException(Exception):
    pass


class WeirdTimeElapsedException(Exception):
    pass


class PhoneDesactivated(Exception):
    pass
