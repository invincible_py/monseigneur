class ProductNotAvailable(Exception):
    pass


class ProductNotFound(Exception):
    pass


class NoShopFound(Exception):
    pass


class ShopDesactivated(Exception):
    pass


class HasLogin(Exception):
    pass


class SubdomainDesactivated(Exception):
    pass


class CategoryDesactivated(Exception):
    pass


class NoChildFound(Exception):
    pass


class AmazonCaptchaException(Exception):
    pass


class FatalCaptchaException(Exception):
    pass


class LastPageReached(Exception):
    pass


class MetaDesactivated(Exception):
    pass

